package org.gabs.statics.solver.matrix.extractor;

import com.google.inject.AbstractModule;

public class SolutionExtractorModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(SolutionExtractor.class).to(SolutionExtractorImpl.class);
    }

}
