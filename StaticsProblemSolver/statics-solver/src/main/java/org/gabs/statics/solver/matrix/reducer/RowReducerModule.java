package org.gabs.statics.solver.matrix.reducer;

import com.google.inject.AbstractModule;

public class RowReducerModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(RowReducer.class).to(GaussianEliminator.class);
    }

}
