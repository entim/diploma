package org.gabs.statics.solver.registry;

import org.gabs.statics.solver.model.Structure;
import org.gabs.statics.solver.model.Constraint;
import org.gabs.statics.solver.model.Element;

import java.util.Collection;

/**
 * {@link ConstraintRegistryFactory} implementation.
 */
class ConstraintRegistryFactoryImpl implements ConstraintRegistryFactory {

    @Override
    public ConstraintRegistry createFrom(Structure structure) {
        ConstraintRegistry result = new ConstraintRegistryImpl();
        for (Element element : structure.getElements()) {
            registerConstraints(structure.getConstraintsOf(element), result);
        }
        return result;
    }

    private void registerConstraints(Collection<Collection<Constraint>> constraintGroups, ConstraintRegistry registry) {
        for (Collection<Constraint> constraintGroup : constraintGroups) {
            for (Constraint constraint : constraintGroup) {
                registry.register(constraint);
            }
        }
    }

}
