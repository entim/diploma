package org.gabs.statics.solver.matrix.builder.processor.constraint;

import org.gabs.statics.solver.model.Constraint;
import org.gabs.statics.solver.model.Dof;
import org.gabs.statics.solver.model.ReferenceFrame;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;
import java.util.Map;

/**
 * Implementation of the {@link ConstraintProcessor} interface that uses {@link ConstraintProcessorStrategy} instances to compute the coefficients for different constrained degrees
 * of freedom.
 */
class ConstraintProcessorImpl implements ConstraintProcessor {

    private List<ConstraintProcessorStrategy> globalConstraintProcessorStrategies;
    private List<ConstraintProcessorStrategy> localConstraintProcessorStrategies;

    @Override
    public void process(Constraint constraint, int index, boolean joint, Map<Dof, double[]> coefficients) {
        List<ConstraintProcessorStrategy> strategies = isGlobal(constraint) ? globalConstraintProcessorStrategies :
                localConstraintProcessorStrategies;
        for (ConstraintProcessorStrategy strategy : strategies) {
            strategy.process(constraint, index, joint, coefficients);
        }
    }

    private boolean isGlobal(Constraint constraint) {
        return ReferenceFrame.GLOBAL.equals(constraint.getLocalReferenceFrame());
    }

    @Inject
    public void setGlobalConstraintProcessorStrategies(@Named("global") List<ConstraintProcessorStrategy> globalConstraintProcessorStrategies) {
        this.globalConstraintProcessorStrategies = globalConstraintProcessorStrategies;
    }

    @Inject
    public void setLocalConstraintProcessorStrategies(@Named("local") List<ConstraintProcessorStrategy> localConstraintProcessorStrategies) {
        this.localConstraintProcessorStrategies = localConstraintProcessorStrategies;
    }

}
