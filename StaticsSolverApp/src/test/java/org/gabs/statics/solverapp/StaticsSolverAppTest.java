package org.gabs.statics.solverapp;

import com.google.gson.Gson;
import org.gabs.statics.model.common.Vector;
import org.gabs.statics.model.common.ConcentratedForce;
import org.gabs.statics.model.result.Reaction;
import org.gabs.statics.model.result.Result;
import org.gabs.statics.model.result.StaticalDeterminacy;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Map;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class StaticsSolverAppTest {

    private String source;
    private String target;

    @Before
    public void setup() throws URISyntaxException {
        source = Paths.get(getClass().getResource("/simply_supported_beam.json").toURI()).toString();
        target = Paths.get(getClass().getResource("/").toURI()).toString() + "/result.json";
    }

    @After
    public void deleteResult() throws IOException {
        Files.delete(FileSystems.getDefault().getPath(target));
    }


    @Test
    public void testWithSimplySupportedBeam() throws URISyntaxException, IOException {
        StaticsSolverApp.main(new String[]{source, target});
        shouldHaveCorrectResult();
    }

    private void shouldHaveCorrectResult() throws IOException {
        Vector endpoint1 = new Vector(0, 0, 0);
        Vector endpoint2 = new Vector(0, 2, 0);

        Reaction expectedReactionOnRollerJointFromBeam = new Reaction("beam", new ConcentratedForce(endpoint1, new Vector(1.5, 0, 0)), null);
        Reaction expectedReactionOnRollerJointFromSupport = new Reaction("ground", new ConcentratedForce(endpoint1, new Vector(-1.5, 0, 0)), null);
        Reaction expectedReactionOnPinnedJointFromBeam = new Reaction("beam", new ConcentratedForce(endpoint2, new Vector(-3.5, -2, 0)), null);
        Reaction expectedReactionOnPinnedJointFromSupport = new Reaction("ground", new ConcentratedForce(endpoint2, new Vector(3.5, 2, 0)), null);
        Reaction expectedReactionOnBeamFromRollerJoint = new Reaction("roller_joint", new ConcentratedForce(endpoint1, new Vector(-1.5, 0, 0)), null);
        Reaction expectedReactionOnBeamFromPinnedJoint = new Reaction("pinned_joint", new ConcentratedForce(endpoint2, new Vector(3.5, 2, 0)), null);

        Result result = deserializeResult();
        assertThat(result.getStructureDeterminacy(), equalTo(StaticalDeterminacy.DETERMINATE));
        assertThat(result.getProblemDeterminacy(), equalTo(StaticalDeterminacy.DETERMINATE));

        Map<String, Collection<Reaction>> reactions = result.getReactions();
        assertThat(reactions.keySet(), containsInAnyOrder("roller_joint", "pinned_joint", "beam"));
        assertThat(reactions.get("roller_joint"), containsInAnyOrder(expectedReactionOnRollerJointFromBeam,
                expectedReactionOnRollerJointFromSupport));
        assertThat(reactions.get("pinned_joint"), containsInAnyOrder(expectedReactionOnPinnedJointFromBeam,
                expectedReactionOnPinnedJointFromSupport));
        assertThat(reactions.get("beam"), containsInAnyOrder(expectedReactionOnBeamFromPinnedJoint, expectedReactionOnBeamFromRollerJoint));
    }

    private Result deserializeResult() throws IOException {
        String json = new String(Files.readAllBytes(FileSystems.getDefault().getPath(target)));
        Gson gson = new Gson();
        return gson.fromJson(json, Result.class);
    }

}
