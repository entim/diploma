package org.gabs.statics.model.result;

import org.gabs.statics.model.common.ConcentratedForce;
import org.gabs.statics.model.common.Moment;

import java.util.Objects;

/**
 * Represents a reaction on a structural element.
 */
public class Reaction {

    private String source;
    private ConcentratedForce concentratedForce;
    private Moment moment;

    /**
     * Creates a new {@link Reaction} instance with the given source and forces.
     * @param source ID of the structural element that applies the action
     * @param concentratedForce a concentrated force
     * @param moment a concentrated moment
     */
    public Reaction(String source, ConcentratedForce concentratedForce, Moment moment) {
        this.source = source;
        this.concentratedForce = concentratedForce;
        this.moment = moment;
    }

    @Override
    public int hashCode() {
        return Objects.hash(source, concentratedForce, moment);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final Reaction other = (Reaction) obj;
        return Objects.equals(this.source, other.source) && Objects.equals(this.concentratedForce, other.concentratedForce) && Objects.equals(this
                .moment, other.moment);
    }

    @Override
    public String toString() {
        return "Reaction{" + "source=" + source + ", concentratedForce=" + concentratedForce + ", concentratedMoment=" + moment + '}';
    }

    public String getSource() {
        return source;
    }

    public ConcentratedForce getConcentratedForce() {
        return concentratedForce;
    }

    public Moment getMoment() {
        return moment;
    }

}
