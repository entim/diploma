package org.gabs.statics.solver.converter.problem;

import org.gabs.statics.model.problem.Beam;
import org.gabs.statics.model.problem.Constraints;
import org.gabs.statics.model.problem.Joint;
import org.gabs.statics.solver.converter.Converter;
import org.gabs.statics.solver.data.Pair;
import org.gabs.statics.solver.data.Vector;
import org.gabs.statics.solver.model.Constraint;
import org.gabs.statics.solver.model.Dof;
import org.gabs.statics.solver.model.ReferenceFrame;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * A {@link Converter} implementation that converts {@code StaticsApi} {@link Constraints} to internal {@link Constraint} representation.
 */
class ConstraintsConverter implements Converter<Pair<Joint, Beam>, Collection<Constraint>> {

    private static final String ID_SEPARATOR = ":";
    private static final String GROUND = "ground";

    private Converter<org.gabs.statics.model.common.Vector, Vector> vectorConverter;
    private Converter<org.gabs.statics.model.common.Vector, ReferenceFrame> referenceFrameConverter;

    @Override
    public Collection<Constraint> convert(Pair<Joint, Beam> source) {
        List<Constraint> result = new ArrayList<>(3);
        Joint joint = source.getLeft();
        String id = getId(source);
        Vector position = getPosition(joint);
        Constraints constraints = source.getRight() != null ? joint.getConstraints() : joint.getSupport();
        addConstraints(result, constraints, id, position);
        return result;
    }

    private String getId(Pair<Joint, Beam> source) {
        return source.getLeft().getId() + ID_SEPARATOR + (source.getRight() != null ? source.getRight().getId() : GROUND);
    }

    private Vector getPosition(Joint source) {
        return vectorConverter.convert(source.getPosition());
    }

    private void addConstraints(List<Constraint> result, Constraints constraints, String id, Vector position) {
        if (constraints != null) {
            ReferenceFrame localReferenceFrame = getLocalReferenceFrame(constraints);
            if (constraints.isConstrainedRz()) {
                result.add(new Constraint(id, Dof.ZZ, localReferenceFrame, position));
            }
            if (constraints.isConstrainedTx()) {
                result.add(new Constraint(id, Dof.X, localReferenceFrame, position));
            }
            if (constraints.isConstrainedTy()) {
                result.add(new Constraint(id, Dof.Y, localReferenceFrame, position));
            }
        }
    }

    private ReferenceFrame getLocalReferenceFrame(Constraints constraints) {
        return referenceFrameConverter.convert(constraints.getLocalReferenceFrameI());
    }

    @Inject
    public void setVectorConverter(Converter<org.gabs.statics.model.common.Vector, Vector> vectorConverter) {
        this.vectorConverter = vectorConverter;
    }

    @Inject
    public void setReferenceFrameConverter(Converter<org.gabs.statics.model.common.Vector, ReferenceFrame> referenceFrameConverter) {
        this.referenceFrameConverter = referenceFrameConverter;
    }

}
