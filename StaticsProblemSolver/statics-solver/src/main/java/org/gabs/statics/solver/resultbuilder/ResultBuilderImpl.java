package org.gabs.statics.solver.resultbuilder;

import org.gabs.statics.model.result.Reaction;
import org.gabs.statics.model.result.Result;
import org.gabs.statics.model.result.StaticalDeterminacy;
import org.gabs.statics.solver.converter.Converter;
import org.gabs.statics.solver.data.Analysis;
import org.gabs.statics.solver.model.Determinacy;
import org.gabs.statics.solver.model.Problem;
import org.gabs.statics.solver.registry.ConstraintRegistry;
import org.gabs.statics.solver.resultbuilder.postprocessor.ReactionMappingPostProcessor;

import javax.inject.Inject;
import java.util.Collection;
import java.util.Map;

/**
 * {@link ResultBuilder} implementation.
 * Converts the results to API model. Assembles the reaction mapping that associates reactions to structural elements,
 * and uses {@link ReactionMappingPostProcessor}s to remove zero reactions, and round results to the expected number of decimals.
 */
class ResultBuilderImpl implements ResultBuilder {

    private Converter<Determinacy, StaticalDeterminacy> determinacyConverter;
    private ReactionMappingBuilder reactionMappingBuilder;
    private ReactionMappingPostProcessor reactionMappingPostProcessor;

    @Override
    public Result build(double[] solution, Analysis analysis, Problem problem, ConstraintRegistry constraintRegistry) {
        Map<String, Collection<Reaction>> reactions = reactionMappingBuilder.build(solution, problem, constraintRegistry);
        StaticalDeterminacy structuralDeterminacy = determinacyConverter.convert(analysis.getStructureDeterminacy());
        StaticalDeterminacy problemDeterminacy = determinacyConverter.convert(analysis.getProblemDeterminacy());
        reactionMappingPostProcessor.postProcess(reactions, problem.getPrecision());
        return new Result(reactions, structuralDeterminacy, problemDeterminacy);
    }

    @Override
    public Result build(Analysis analysis) {
        StaticalDeterminacy structuralDeterminacy = determinacyConverter.convert(analysis.getStructureDeterminacy());
        StaticalDeterminacy problemDeterminacy = determinacyConverter.convert(analysis.getProblemDeterminacy());
        return new Result(structuralDeterminacy, problemDeterminacy);
    }

    @Override
    public Result build(String error) {
        return new Result(error);
    }

    @Inject
    public void setDeterminacyConverter(Converter<Determinacy, StaticalDeterminacy> determinacyConverter) {
        this.determinacyConverter = determinacyConverter;
    }

    @Inject
    public void setReactionMappingBuilder(ReactionMappingBuilder reactionMappingBuilder) {
        this.reactionMappingBuilder = reactionMappingBuilder;
    }

    @Inject
    public void setReactionMappingPostProcessor(ReactionMappingPostProcessor reactionMappingPostProcessor) {
        this.reactionMappingPostProcessor = reactionMappingPostProcessor;
    }

}
