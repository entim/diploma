/**
 * Defines classes and interfaces to transform an internal result to {@code StaticsApi} representation.
 */
package org.gabs.statics.solver.converter.result;