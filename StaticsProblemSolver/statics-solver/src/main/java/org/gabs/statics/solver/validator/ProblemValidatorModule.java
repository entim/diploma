package org.gabs.statics.solver.validator;

import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;

import java.util.Arrays;
import java.util.List;

public class ProblemValidatorModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(ProblemValidator.class).to(ChainedProblemValidator.class);
        bind(new TypeLiteral<List<ProblemValidator>>() {}).toInstance(Arrays.asList(new IdUniquenessValidator(),
                new StructuralContinuityValidator()));
    }

}
