package org.gabs.statics.solver.converter.result;

import org.gabs.statics.model.result.StaticalDeterminacy;
import org.gabs.statics.solver.converter.Converter;
import org.gabs.statics.solver.model.Determinacy;

/**
 * A {@link Converter} implementation that converts an internal representation of {@link Determinacy} to {@code StaticsApi} {@link StaticalDeterminacy}.
 */
class DeterminacyConverter implements Converter<Determinacy, StaticalDeterminacy> {

    @Override
    public StaticalDeterminacy convert(Determinacy source) {
        StaticalDeterminacy result;
        if (Determinacy.DETERMINATE == source) {
            result = StaticalDeterminacy.DETERMINATE;
        } else if (Determinacy.OVERDETERMINATE == source) {
            result = StaticalDeterminacy.OVERDETERMINATE;
        } else {
            result = StaticalDeterminacy.INDETERMINATE;
        }
        return result;
    }

}
