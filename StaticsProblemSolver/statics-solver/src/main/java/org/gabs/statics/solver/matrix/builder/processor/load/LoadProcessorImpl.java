package org.gabs.statics.solver.matrix.builder.processor.load;

import org.gabs.statics.solver.model.Dof;
import org.gabs.statics.solver.model.Effect;

import javax.inject.Inject;
import java.util.List;
import java.util.Map;

/**
 * Implementation of the {@link LoadProcessor} interface that uses a {@link LoadProcessorStrategy} list to compute the effect of the given load.
 */
class LoadProcessorImpl implements LoadProcessor {

    private List<LoadProcessorStrategy> loadProcessorStrategies;

    @Override
    public void process(Effect load, Map<Dof, double[]> coefficients, boolean joint) {
        for (LoadProcessorStrategy strategy : loadProcessorStrategies) {
            strategy.process(load, coefficients, joint);
        }
    }

    @Inject
    public void setLoadProcessorStrategies(List<LoadProcessorStrategy> loadProcessorStrategies) {
        this.loadProcessorStrategies = loadProcessorStrategies;
    }

}
