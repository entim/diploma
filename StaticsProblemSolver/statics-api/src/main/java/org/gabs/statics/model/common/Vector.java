package org.gabs.statics.model.common;

import java.util.Objects;

/**
 * Represents a three dimensional vector.
 */
public class Vector {

    /**
     * Represents the null vector.
     */
    public static final Vector NULL = new Vector(0, 0, 0);

    private double x;
    private double y;
    private double z;

    /**
     * Creates a new vector that has the given projections on {@code x},  {@code x} and  {@code z} directions.
     * @param x the vector's projection on {@code x} direction
     * @param y the vector's projection on {@code y} direction
     * @param z the vector's projection on {@code z} direction
     */
    public Vector(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y, z);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final Vector other = (Vector) obj;
        return this.x == other.x && this.y == other.y && this.z == other.z;
    }

    @Override
    public String toString() {
        return "Vector{" + "x=" + x + ", y=" + y + ", z=" + z + '}';
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

}
