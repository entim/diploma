package org.gabs.statics.model.result;

/**
 * An enumeration of possible statical determinacies.
 */
public enum StaticalDeterminacy {

    DETERMINATE, INDETERMINATE, OVERDETERMINATE

}
