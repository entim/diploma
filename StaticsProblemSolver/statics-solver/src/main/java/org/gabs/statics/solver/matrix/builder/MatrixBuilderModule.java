package org.gabs.statics.solver.matrix.builder;

import com.google.inject.AbstractModule;

public class MatrixBuilderModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(MatrixBuilder.class).to(MatrixBuilderImpl.class);
    }

}
