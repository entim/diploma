package org.gabs.statics.solver.matrix.builder;

import com.google.inject.Guice;
import com.google.inject.Injector;
import org.gabs.statics.solver.data.Vector;
import org.gabs.statics.solver.matrix.builder.processor.constraint.ConstraintProcessorModule;
import org.gabs.statics.solver.matrix.builder.processor.ElementProcessorModule;
import org.gabs.statics.solver.matrix.builder.processor.load.LoadProcessorModule;
import org.gabs.statics.solver.model.ConcentratedForce;
import org.gabs.statics.solver.model.ConcentratedMoment;
import org.gabs.statics.solver.model.Constraint;
import org.gabs.statics.solver.model.Dof;
import org.gabs.statics.solver.model.Effect;
import org.gabs.statics.solver.model.Element;
import org.gabs.statics.solver.model.Problem;
import org.gabs.statics.solver.model.ReferenceFrame;
import org.gabs.statics.solver.model.Structure;
import org.gabs.statics.solver.registry.ConstraintRegistry;
import org.gabs.statics.solver.registry.ConstraintRegistryModule;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class MatrixBuilderComponentTest {

    private static final int PRECISION = 14;
    private static final double SQRT2 = Math.sqrt(2);
    private static final double SQRT2DIV2 = SQRT2 / 2.0d;

    private ConstraintRegistry constraintRegistry;
    private MatrixBuilder builder;
    private Element rollerJoint;
    private Element pinnedJoint;
    private Element fixedJoint;
    private Element beam;
    private Constraint rollerXSupport;
    private Constraint rollerX;
    private Constraint pinnedXSupport;
    private Constraint pinnedYSupport;
    private Constraint pinnedX;
    private Constraint pinnedY;
    private Constraint fixedXSupport;
    private Constraint fixedYSupport;
    private Constraint fixedZZSupport;
    private Constraint fixedX;
    private Constraint fixedY;
    private Constraint fixedZZ;
    private double[][] result;

    @Test
    public void testBuildWithSimplySupportedBeam() {
        givenMatrixBuilder();
        whenCalledWithSimplySupportedBeam();
        thenResultIsCorrectMatrixForSimplySupportedBeam();
    }

    @Test
    public void testBuildWithCantilever() {
        givenMatrixBuilder();
        whenCalledWithCantilever();
        thenResultIsCorrectMatrixForCantilever();
    }

    @Test
    public void testBuildWithSimplySupportedBeamWithJointsInLocalReferenceFrame() {
        givenMatrixBuilder();
        whenCalledWithSimplySupportedBeamWithJointsInLocalReferenceFrame();
        thenResultIsCorrectMatrixForSimplySupportedBeamWithJointsInLocalReferenceFrame();
    }

    private void givenMatrixBuilder() {
        Injector injector = Guice.createInjector(new MatrixBuilderModule(), new ElementProcessorModule(), new ConstraintProcessorModule(),
                new LoadProcessorModule(), new ConstraintRegistryModule());
        constraintRegistry = injector.getInstance(ConstraintRegistry.class);
        builder = injector.getInstance(MatrixBuilder.class);
    }

    private void whenCalledWithSimplySupportedBeam() {
        Structure structure = createSimplySupportedBeam();
        Map<Element, Collection<Effect>> load = createLoadForSimplySupportedBeam();
        Problem problem = new Problem(structure, load, PRECISION);
        result = builder.build(problem, constraintRegistry);
    }

    private Structure createSimplySupportedBeam() {
        rollerJoint = new Element("roller joint", true);
        pinnedJoint = new Element("pinned joint", true);
        beam = new Element("beam", false);
        rollerXSupport = new Constraint("roller X support", Dof.X, new Vector(0, 0, 0));
        rollerX = new Constraint("roller X", Dof.X, new Vector(0, 0, 0));
        pinnedXSupport = new Constraint("pinned X support", Dof.X, new Vector(0, 2, 0));
        pinnedYSupport = new Constraint("pinned Y support", Dof.Y, new Vector(0, 2, 0));
        pinnedX = new Constraint("pinned X", Dof.X, new Vector(0, 2, 0));
        pinnedY = new Constraint("pinned Y", Dof.Y, new Vector(0, 2, 0));

        initializeConstraintRegistryForSimplySupportedBeam();

        Structure result = new Structure();
        result.addExternalConstraints(rollerJoint, Collections.singleton(rollerXSupport));
        result.addInternalConstraints(rollerJoint, beam, Collections.singleton(rollerX));
        result.addExternalConstraints(pinnedJoint, Arrays.asList(pinnedXSupport, pinnedYSupport));
        result.addInternalConstraints(pinnedJoint, beam, Arrays.asList(pinnedX, pinnedY));
        result.addInternalConstraints(beam, rollerJoint, Collections.singleton(rollerX));
        result.addInternalConstraints(beam, pinnedJoint, Arrays.asList(pinnedX, pinnedY));
        return result;
    }

    private void initializeConstraintRegistryForSimplySupportedBeam() {
        constraintRegistry.register(rollerXSupport);
        constraintRegistry.register(rollerX);
        constraintRegistry.register(pinnedXSupport);
        constraintRegistry.register(pinnedYSupport);
        constraintRegistry.register(pinnedX);
        constraintRegistry.register(pinnedY);
    }

    private Map<Element, Collection<Effect>> createLoadForSimplySupportedBeam() {
        Effect concentratedForce = new ConcentratedForce(new Vector(0, 1, 0), new Vector(-2, -2, 0));
        Effect concentratedMoment = new ConcentratedMoment(new Vector(0, 0, 5));
        return Collections.<Element, Collection<Effect>>singletonMap(beam, Arrays.asList(concentratedForce, concentratedMoment));
    }

    private void thenResultIsCorrectMatrixForSimplySupportedBeam() {
        int rollerXSupportIdx = constraintRegistry.getIndex(rollerXSupport);
        int rollerXIdx = constraintRegistry.getIndex(rollerX);
        int pinnedXSupportIdx = constraintRegistry.getIndex(pinnedXSupport);
        int pinnedYSupportIdx = constraintRegistry.getIndex(pinnedYSupport);
        int pinnedXIdx = constraintRegistry.getIndex(pinnedX);
        int pinnedYIdx = constraintRegistry.getIndex(pinnedY);

        double[] expectedRollerJointXCoefficients = new double[7];
        expectedRollerJointXCoefficients[rollerXSupportIdx] = 1;
        expectedRollerJointXCoefficients[rollerXIdx] = 1;

        double[] expectedPinnedJointXCoefficients = new double[7];
        expectedPinnedJointXCoefficients[pinnedXSupportIdx] = 1;
        expectedPinnedJointXCoefficients[pinnedXIdx] = 1;

        double[] expectedPinnedJointYCoefficients = new double[7];
        expectedPinnedJointYCoefficients[pinnedYSupportIdx] = 1;
        expectedPinnedJointYCoefficients[pinnedYIdx] = 1;

        double[] beamXCoefficients = new double[7];
        beamXCoefficients[rollerXIdx] = -1;
        beamXCoefficients[pinnedXIdx] = -1;
        beamXCoefficients[6] = 2;

        double[] beamYCoefficients = new double[7];
        beamYCoefficients[pinnedYIdx] = -1;
        beamYCoefficients[6] = 2;

        double[] beamZZCoefficients = new double[7];
        beamZZCoefficients[pinnedXIdx] = 2;
        beamZZCoefficients[6] = -7;

        assertThat(result.length, equalTo(6));
        assertThat(result, hasItemInArray(expectedRollerJointXCoefficients));
        assertThat(result, hasItemInArray(expectedPinnedJointXCoefficients));
        assertThat(result, hasItemInArray(expectedPinnedJointYCoefficients));
        assertThat(result, hasItemInArray(beamXCoefficients));
        assertThat(result, hasItemInArray(beamYCoefficients));
        assertThat(result, hasItemInArray(beamZZCoefficients));
    }

    private void whenCalledWithCantilever() {
        Structure structure = createCantilever();
        Map<Element, Collection<Effect>> load = createLoadForCantilever();
        Problem problem = new Problem(structure, load, PRECISION);
        result = builder.build(problem, constraintRegistry);
    }

    private Structure createCantilever() {
        fixedJoint = new Element("fixed joint", true);
        beam = new Element("beam", false);
        fixedXSupport = new Constraint("fixed X support", Dof.X, new Vector(0, 0, 0));
        fixedYSupport = new Constraint("fixed Y support", Dof.Y, new Vector(0, 0, 0));
        fixedZZSupport = new Constraint("fixed ZZ support", Dof.ZZ, new Vector(0, 0, 0));
        fixedX = new Constraint("fixed X", Dof.X, new Vector(0, 0, 0));
        fixedY = new Constraint("fixed Y", Dof.Y, new Vector(0, 0, 0));
        fixedZZ = new Constraint("fixed ZZ", Dof.ZZ, new Vector(0, 0, 0));

        initializeConstraintRegistryForCantilever();

        Structure result = new Structure();
        result.addExternalConstraints(fixedJoint, Arrays.asList(fixedXSupport, fixedYSupport, fixedZZSupport));
        result.addInternalConstraints(fixedJoint, beam, Arrays.asList(fixedX, fixedY, fixedZZ));
        result.addInternalConstraints(beam, fixedJoint, Arrays.asList(fixedX, fixedY, fixedZZ));
        return result;
    }

    private void initializeConstraintRegistryForCantilever() {
        constraintRegistry.register(fixedXSupport);
        constraintRegistry.register(fixedYSupport);
        constraintRegistry.register(fixedZZSupport);
        constraintRegistry.register(fixedX);
        constraintRegistry.register(fixedY);
        constraintRegistry.register(fixedZZ);
    }

    private Map<Element, Collection<Effect>> createLoadForCantilever() {
        Effect concentratedForce = new ConcentratedForce(new Vector(2, 2, 0), new Vector(-2, 2, 0));
        Effect concentratedMoment = new ConcentratedMoment(new Vector(0, 0, 5));
        return Collections.<Element, Collection<Effect>>singletonMap(beam, Arrays.asList(concentratedForce, concentratedMoment));
    }

    private void thenResultIsCorrectMatrixForCantilever() {
        int fixedXSupportIdx = constraintRegistry.getIndex(fixedXSupport);
        int fixedYSupportIdx = constraintRegistry.getIndex(fixedYSupport);
        int fixedZZSupportIdx = constraintRegistry.getIndex(fixedZZSupport);
        int fixedXIdx = constraintRegistry.getIndex(fixedX);
        int fixedYIdx = constraintRegistry.getIndex(fixedY);
        int fixedZZIdx = constraintRegistry.getIndex(fixedZZ);

        double[] expectedJointXCoefficients = new double[7];
        expectedJointXCoefficients[fixedXSupportIdx] = 1;
        expectedJointXCoefficients[fixedXIdx] = 1;

        double[] expectedJointYCoefficients = new double[7];
        expectedJointYCoefficients[fixedYSupportIdx] = 1;
        expectedJointYCoefficients[fixedYIdx] = 1;

        double[] expectedJointZZCoefficients = new double[7];
        expectedJointZZCoefficients[fixedZZSupportIdx] = 1;
        expectedJointZZCoefficients[fixedZZIdx] = 1;

        double[] expectedBeamXCoefficients = new double[7];
        expectedBeamXCoefficients[fixedXIdx] = -1;
        expectedBeamXCoefficients[6] = 2;

        double[] expectedBeamYCoefficients = new double[7];
        expectedBeamYCoefficients[fixedYIdx] = -1;
        expectedBeamYCoefficients[6] = -2;

        double[] expectedBeamZZCoefficients = new double[7];
        expectedBeamZZCoefficients[fixedZZIdx] = -1;
        expectedBeamZZCoefficients[6] = -13;

        assertThat(result.length, equalTo(6));
        assertThat(result, hasItemInArray(expectedJointXCoefficients));
        assertThat(result, hasItemInArray(expectedJointYCoefficients));
        assertThat(result, hasItemInArray(expectedJointZZCoefficients));
        assertThat(result, hasItemInArray(expectedBeamXCoefficients));
        assertThat(result, hasItemInArray(expectedBeamYCoefficients));
        assertThat(result, hasItemInArray(expectedBeamZZCoefficients));
    }

    private void whenCalledWithSimplySupportedBeamWithJointsInLocalReferenceFrame() {
        Structure structure = createSimplySupportedBeamWithJointsInLocalReferenceFrame();
        Map<Element, Collection<Effect>> load = createLoadForSimplySupportedBeamWithJointsInLocalReferenceFrame();
        Problem problem = new Problem(structure, load, PRECISION);
        result = builder.build(problem, constraintRegistry);
    }

    private Structure createSimplySupportedBeamWithJointsInLocalReferenceFrame() {
        ReferenceFrame rollerJointReferenceFrame = new ReferenceFrame(new Vector(SQRT2DIV2, SQRT2DIV2, 0), new Vector(-SQRT2DIV2, SQRT2DIV2, 0),
                new Vector(0, 0, 1));
        ReferenceFrame pinnedJointReferenceFrame = new ReferenceFrame(new Vector(SQRT2DIV2, -SQRT2DIV2, 0), new Vector(SQRT2DIV2, SQRT2DIV2, 0),
                new Vector(0, 0, 1));

        rollerJoint = new Element("roller joint", true);
        pinnedJoint = new Element("pinned joint", true);
        beam = new Element("beam", false);
        rollerXSupport = new Constraint("roller X support", Dof.X, rollerJointReferenceFrame, new Vector(0, 0, 0));
        rollerX = new Constraint("roller X", Dof.X, rollerJointReferenceFrame, new Vector(0, 0, 0));
        pinnedXSupport = new Constraint("pinned X support", Dof.X, pinnedJointReferenceFrame, new Vector(0, 2, 0));
        pinnedYSupport = new Constraint("pinned Y support", Dof.Y, pinnedJointReferenceFrame, new Vector(0, 2, 0));
        pinnedX = new Constraint("pinned X", Dof.X, pinnedJointReferenceFrame, new Vector(0, 2, 0));
        pinnedY = new Constraint("pinned Y", Dof.Y, pinnedJointReferenceFrame, new Vector(0, 2, 0));

        initializeConstraintRegistryForSimplySupportedBeam();

        Structure result = new Structure();
        result.addExternalConstraints(rollerJoint, Collections.singleton(rollerXSupport));
        result.addInternalConstraints(rollerJoint, beam, Collections.singleton(rollerX));
        result.addExternalConstraints(pinnedJoint, Arrays.asList(pinnedXSupport, pinnedYSupport));
        result.addInternalConstraints(pinnedJoint, beam, Arrays.asList(pinnedX, pinnedY));
        result.addInternalConstraints(beam, rollerJoint, Collections.singleton(rollerX));
        result.addInternalConstraints(beam, pinnedJoint, Arrays.asList(pinnedX, pinnedY));
        return result;
    }

    private Map<Element, Collection<Effect>> createLoadForSimplySupportedBeamWithJointsInLocalReferenceFrame() {
        Effect concentratedForce = new ConcentratedForce(new Vector(0, 1, 0), new Vector(-2, -2, 0));
        Effect concentratedMoment = new ConcentratedMoment(new Vector(0, 0, 5));
        return Collections.<Element, Collection<Effect>>singletonMap(beam, Arrays.asList(concentratedForce, concentratedMoment));
    }

    private void thenResultIsCorrectMatrixForSimplySupportedBeamWithJointsInLocalReferenceFrame() {
        int rollerXSupportIdx = constraintRegistry.getIndex(rollerXSupport);
        int rollerXIdx = constraintRegistry.getIndex(rollerX);
        int pinnedXSupportIdx = constraintRegistry.getIndex(pinnedXSupport);
        int pinnedYSupportIdx = constraintRegistry.getIndex(pinnedYSupport);
        int pinnedXIdx = constraintRegistry.getIndex(pinnedX);
        int pinnedYIdx = constraintRegistry.getIndex(pinnedY);

        double[] expectedRollerJointXCoefficients = new double[7];
        expectedRollerJointXCoefficients[rollerXSupportIdx] = SQRT2DIV2;
        expectedRollerJointXCoefficients[rollerXIdx] = SQRT2DIV2;

        double[] expectedRollerJointYCoefficients = new double[7];
        expectedRollerJointYCoefficients[rollerXSupportIdx] = SQRT2DIV2;
        expectedRollerJointYCoefficients[rollerXIdx] = SQRT2DIV2;

        double[] expectedPinnedJointXCoefficients = new double[7];
        expectedPinnedJointXCoefficients[pinnedXSupportIdx] = -SQRT2DIV2;
        expectedPinnedJointXCoefficients[pinnedYSupportIdx] = SQRT2DIV2;
        expectedPinnedJointXCoefficients[pinnedXIdx] = -SQRT2DIV2;
        expectedPinnedJointXCoefficients[pinnedYIdx] = SQRT2DIV2;

        double[] expectedPinnedJointYCoefficients = new double[7];
        expectedPinnedJointYCoefficients[pinnedXSupportIdx] = SQRT2DIV2;
        expectedPinnedJointYCoefficients[pinnedYSupportIdx] = SQRT2DIV2;
        expectedPinnedJointYCoefficients[pinnedXIdx] = SQRT2DIV2;
        expectedPinnedJointYCoefficients[pinnedYIdx] = SQRT2DIV2;

        double[] beamXCoefficients = new double[7];
        beamXCoefficients[rollerXIdx] = -SQRT2DIV2;
        beamXCoefficients[pinnedXIdx] = SQRT2DIV2;
        beamXCoefficients[pinnedYIdx] = -SQRT2DIV2;
        beamXCoefficients[6] = 2;

        double[] beamYCoefficients = new double[7];
        beamYCoefficients[rollerXIdx] = -SQRT2DIV2;
        beamYCoefficients[pinnedXIdx] = -SQRT2DIV2;
        beamYCoefficients[pinnedYIdx] = -SQRT2DIV2;
        beamYCoefficients[6] = 2;

        double[] beamZZCoefficients = new double[7];
        beamZZCoefficients[pinnedXIdx] = SQRT2;
        beamZZCoefficients[pinnedYIdx] = SQRT2;
        beamZZCoefficients[6] = -7;

        assertThat(result.length, equalTo(7));
        assertThat(result, hasItemInArray(expectedRollerJointXCoefficients));
        assertThat(result, hasItemInArray(expectedRollerJointYCoefficients));
        assertThat(result, hasItemInArray(expectedPinnedJointXCoefficients));
        assertThat(result, hasItemInArray(expectedPinnedJointYCoefficients));
        assertThat(result, hasItemInArray(beamXCoefficients));
        assertThat(result, hasItemInArray(beamYCoefficients));
        assertThat(result, hasItemInArray(beamZZCoefficients));
    }

    private Matcher<double[][]> hasItemInArray(final double[] element) {
        return new TypeSafeMatcher<double[][]>() {
            @Override
            protected boolean matchesSafely(double[][] item) {
                boolean result = false;
                for (double[] row : item) {
                    if (matches(element, row)) {
                        result = true;
                        break;
                    }
                }
                return result;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("Array should contain ").appendValue(Arrays.toString(element));
            }

            private boolean matches(double[] array1, double[] array2) {
                boolean result = true;
                for (int i = 0; i < array1.length; i++) {
                    if (array1[i] != array2[i]) {
                        result = false;
                        break;
                    }
                }
                return result;
            }
        };
    }

}
