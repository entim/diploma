package org.gabs.statics.solver.matrix.builder.processor.constraint;

import org.gabs.statics.solver.model.Constraint;
import org.gabs.statics.solver.model.Dof;

import java.util.Map;

/**
 * Implementation of the {@link ConstraintProcessorStrategy} interface that processes constraints on the direction of X translation, defined in global reference frame.
 */
class GlobalXConstraintProcessorStrategy implements ConstraintProcessorStrategy {

    @Override
    public void process(Constraint constraint, int index, boolean joint, Map<Dof, double[]> coefficients) {
        if (Dof.X == constraint.getConstrainedDisplacement()) {
            int coefficient = joint ? 1 : -1;
            coefficients.get(Dof.X)[index] = coefficient;
        }
    }

}
