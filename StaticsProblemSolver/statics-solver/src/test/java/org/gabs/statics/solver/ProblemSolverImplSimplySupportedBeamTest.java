package org.gabs.statics.solver;

import org.gabs.statics.model.common.ConcentratedForce;
import org.gabs.statics.model.common.Vector;
import org.gabs.statics.model.problem.*;
import org.gabs.statics.model.common.Moment;
import org.gabs.statics.model.result.Ground;
import org.gabs.statics.model.result.Reaction;
import org.gabs.statics.model.result.Result;
import org.gabs.statics.model.result.StaticalDeterminacy;
import org.junit.Test;

import java.util.Collection;
import java.util.Map;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

public class ProblemSolverImplSimplySupportedBeamTest {

    private static final double SQRT2 = Math.sqrt(2);
    private static final double SQRT2DIV2 = SQRT2 / 2.0d;

    private ProblemSolver solver;
    private Joint rollerJoint1;
    private Joint rollerJoint2;
    private Joint pinnedJoint1;
    private Joint pinnedJoint2;
    private Beam beam;
    private Result result;

    @Test
    public void testSolveDeterminateSimplySupportedBeam() {
        givenProblemSolver();
        whenCalledWithDeterminateSimplySupportedBeam();
        thenResultHasCorrectReactionsForDeterminateSimplySupportedBeam();
        thenResultHasDeterminateStructuralDeterminacy();
        thenResultHasDeterminateProblemDeterminacy();
        thenResultHasNullError();
    }

    @Test
    public void testSolveSolvableOverdeterminateSimplySupportedBeam() {
        givenProblemSolver();
        whenCalledWithSolvableOverdeterminateSimplySupportedBeam();
        thenResultHasCorrectReactionsForSolvableOverdeterminateSimplySupportedBeam();
        thenResultHasOverdeterminateStructuralDeterminacy();
        thenResultHasDeterminateProblemDeterminacy();
        thenResultHasNullError();
    }

    @Test
    public void testSolveUnsolvableOverdeterminateSimplySupportedBeam() {
        givenProblemSolver();
        whenCalledWithUnsolvableOverdeterminateSimplySupportedBeam();
        thenResultHasNullReactions();
        thenResultHasOverdeterminateStructuralDeterminacy();
        thenResultHasOverdeterminateProblemDeterminacy();
        thenResultHasNullError();
    }

    @Test
    public void testSolveIndeterminateSimplySupportedBeam() {
        givenProblemSolver();
        whenCalledWithSolvableIndeterminateSimplySupportedBeam();
        thenResultHasNullReactions();
        thenResultHasIndeterminateStructuralDeterminacy();
        thenResultHasIndeterminateProblemDeterminacy();
        thenResultHasNullError();
    }

    @Test
    public void testSolveDeterminateSimplySupportedBeamWithSupportsInLocalReferenceFrame() {
        givenProblemSolver();
        whenCalledWithDeterminateSimplySupportedBeamWithSupportsInLocalReferenceFrame();
        thenResultHasCorrectReactionsForDeterminateSimplySupportedBeamWithSupportsInLocalReferenceFrame();
        thenResultHasDeterminateStructuralDeterminacy();
        thenResultHasDeterminateProblemDeterminacy();
        thenResultHasNullError();
    }

    private void givenProblemSolver() {
        solver = new ProblemSolverFactoryImpl().create();
    }

    private void whenCalledWithDeterminateSimplySupportedBeam() {
        Problem problem = createSimplySupportedBeam();
        result = solver.solve(problem);
    }

    private Problem createSimplySupportedBeam() {
        rollerJoint1 = createRollerJoint1();
        pinnedJoint1 = createPinnedJoint2();
        beam = createBeamForDeterminateProblem();

        Problem.Builder builder = new Problem.Builder();
        builder.addBeam(rollerJoint1, beam);
        builder.addBeam(pinnedJoint1, beam);
        return builder.build();
    }

    private Joint createRollerJoint1() {
        Constraints hinge = new Constraints.Builder().constrainTx().constrainTy().build();
        Constraints rollerSupport = new Constraints.Builder().constrainTx().build();
        Joint.Builder builder = new Joint.Builder("roller joint 1", new Vector(0, 0, 0), hinge);
        builder.withSupport(rollerSupport);
        return builder.build();
    }

    private Joint createRollerJoint2() {
        Constraints hinge = new Constraints.Builder().constrainTx().constrainTy().build();
        Constraints rollerSupport = new Constraints.Builder().constrainTx().build();
        Joint.Builder builder = new Joint.Builder("roller joint 2", new Vector(0, 2, 0), hinge);
        builder.withSupport(rollerSupport);
        return builder.build();
    }

    private Joint createPinnedJoint1() {
        Constraints hinge = new Constraints.Builder().constrainTx().constrainTy().build();
        Constraints pinnedSupport = new Constraints.Builder().constrainTx().constrainTy().build();
        Joint.Builder builder = new Joint.Builder("pinned joint 1", new Vector(0, 0, 0), hinge);
        builder.withSupport(pinnedSupport);
        return builder.build();
    }

    private Joint createPinnedJoint2() {
        Constraints hinge = new Constraints.Builder().constrainTx().constrainTy().build();
        Constraints pinnedSupport = new Constraints.Builder().constrainTx().constrainTy().build();
        Joint.Builder builder = new Joint.Builder("pinned joint 2", new Vector(0, 2, 0), hinge);
        builder.withSupport(pinnedSupport);
        return builder.build();
    }

    private Beam createBeamForDeterminateProblem() {
        ConcentratedForce concentratedForce = new ConcentratedForce(new Vector(0, 1, 0), new Vector(-2, -2, 0));
        Moment moment = new Moment(new Vector(0, 0, 5));
        Beam.Builder builder = new Beam.Builder("beam");
        builder.addConcentratedForce(concentratedForce);
        builder.addMoment(moment);
        return builder.build();
    }

    private void thenResultHasCorrectReactionsForDeterminateSimplySupportedBeam() {
        Reaction expectedRollerSupportReaction = new Reaction(Ground.getId(), new ConcentratedForce(new Vector(0, 0, 0), new Vector(-1.5, 0, 0)),
                null);
        Reaction expectedRollerFromBeamReaction = new Reaction(beam.getId(), new ConcentratedForce(new Vector(0, 0, 0), new Vector(1.5, 0, 0)), null);
        Reaction expectedPinnedSupportReaction = new Reaction(Ground.getId(), new ConcentratedForce(new Vector(0, 2, 0), new Vector(3.5, 2, 0)),
                null);
        Reaction expectedPinnedFromBeamReaction = new Reaction(beam.getId(), new ConcentratedForce(new Vector(0, 2, 0), new Vector(-3.5, -2, 0)),
                null);
        Reaction expectedBeamFromRollerReaction = new Reaction(rollerJoint1.getId(), new ConcentratedForce(new Vector(0, 0, 0), new Vector(-1.5, 0,
                0)), null);
        Reaction expectedBeamFromPinnedReaction = new Reaction(pinnedJoint1.getId(), new ConcentratedForce(new Vector(0, 2, 0), new Vector(3.5, 2,
                0)), null);

        Map<String, Collection<Reaction>> reactions = result.getReactions();
        assertThat(reactions.keySet(), containsInAnyOrder(rollerJoint1.getId(), pinnedJoint1.getId(), beam.getId()));
        assertThat(reactions.get(rollerJoint1.getId()), containsInAnyOrder(expectedRollerSupportReaction, expectedRollerFromBeamReaction));
        assertThat(reactions.get(pinnedJoint1.getId()), containsInAnyOrder(expectedPinnedSupportReaction, expectedPinnedFromBeamReaction));
        assertThat(reactions.get(beam.getId()), containsInAnyOrder(expectedBeamFromRollerReaction, expectedBeamFromPinnedReaction));
    }

    private void thenResultHasDeterminateStructuralDeterminacy() {
        assertThat(result.getStructureDeterminacy(), equalTo(StaticalDeterminacy.DETERMINATE));
    }

    private void thenResultHasDeterminateProblemDeterminacy() {
        assertThat(result.getProblemDeterminacy(), equalTo(StaticalDeterminacy.DETERMINATE));
    }

    private void thenResultHasNullError() {
        assertThat(result.getError(), nullValue());
    }

    private void whenCalledWithSolvableOverdeterminateSimplySupportedBeam() {
        Problem problem = createSolvableOverdeterminateSimplySupportedBeam();
        result = solver.solve(problem);
    }

    private Problem createSolvableOverdeterminateSimplySupportedBeam() {
        rollerJoint1 = createRollerJoint1();
        rollerJoint2 = createRollerJoint2();
        beam = createBeamForSolvableOverdeterminateProblem();

        Problem.Builder builder = new Problem.Builder();
        builder.addBeam(rollerJoint1, beam);
        builder.addBeam(rollerJoint2, beam);
        return builder.build();
    }

    private Beam createBeamForSolvableOverdeterminateProblem() {
        ConcentratedForce concentratedForce = new ConcentratedForce(new Vector(0, 1, 0), new Vector(-2, 0, 0));
        Beam.Builder builder = new Beam.Builder("beam");
        builder.addConcentratedForce(concentratedForce);
        return builder.build();
    }

    private void thenResultHasCorrectReactionsForSolvableOverdeterminateSimplySupportedBeam() {
        Reaction expectedRoller1SupportReaction = new Reaction(Ground.getId(), new ConcentratedForce(new Vector(0, 0, 0), new Vector(1.0, 0, 0)),
                null);
        Reaction expectedRoller1FromBeamReaction = new Reaction(beam.getId(), new ConcentratedForce(new Vector(0, 0, 0), new Vector(-1.0, 0, 0)),
                null);
        Reaction expectedRoller2SupportReaction = new Reaction(Ground.getId(), new ConcentratedForce(new Vector(0, 2, 0), new Vector(1.0, 0, 0)),
                null);
        Reaction expectedRoller2FromBeamReaction = new Reaction(beam.getId(), new ConcentratedForce(new Vector(0, 2, 0), new Vector(-1.0, 0, 0)),
                null);
        Reaction expectedBeamFromRoller1Reaction = new Reaction(rollerJoint1.getId(), new ConcentratedForce(new Vector(0, 0, 0), new Vector(1.0, 0,
                0)), null);
        Reaction expectedBeamFromRoller2Reaction = new Reaction(rollerJoint2.getId(), new ConcentratedForce(new Vector(0, 2, 0), new Vector(1.0, 0,
                0)), null);

        Map<String, Collection<Reaction>> reactions = result.getReactions();
        assertThat(reactions.keySet(), containsInAnyOrder(rollerJoint1.getId(), rollerJoint2.getId(), beam.getId()));
        assertThat(reactions.get(rollerJoint1.getId()), containsInAnyOrder(expectedRoller1SupportReaction, expectedRoller1FromBeamReaction));
        assertThat(reactions.get(rollerJoint2.getId()), containsInAnyOrder(expectedRoller2SupportReaction, expectedRoller2FromBeamReaction));
        assertThat(reactions.get(beam.getId()), containsInAnyOrder(expectedBeamFromRoller1Reaction, expectedBeamFromRoller2Reaction));
    }

    private void thenResultHasOverdeterminateStructuralDeterminacy() {
        assertThat(result.getStructureDeterminacy(), equalTo(StaticalDeterminacy.OVERDETERMINATE));
    }

    private void whenCalledWithUnsolvableOverdeterminateSimplySupportedBeam() {
        Problem problem = createUnsolvableOverdeterminateSimplySupportedBeam();
        result = solver.solve(problem);
    }

    private Problem createUnsolvableOverdeterminateSimplySupportedBeam() {
        rollerJoint1 = createRollerJoint1();
        rollerJoint2 = createRollerJoint2();
        beam = createBeamForDeterminateProblem();

        Problem.Builder builder = new Problem.Builder();
        builder.addBeam(rollerJoint1, beam);
        builder.addBeam(rollerJoint2, beam);
        return builder.build();
    }

    private void thenResultHasNullReactions() {
        assertThat(result.getReactions(), nullValue());
    }

    private void thenResultHasOverdeterminateProblemDeterminacy() {
        assertThat(result.getProblemDeterminacy(), equalTo(StaticalDeterminacy.OVERDETERMINATE));
    }

    private void whenCalledWithSolvableIndeterminateSimplySupportedBeam() {
        Problem problem = createSolvableIndeterminateSimplySupportedBeam();
        result = solver.solve(problem);
    }

    private Problem createSolvableIndeterminateSimplySupportedBeam() {
        pinnedJoint1 = createPinnedJoint1();
        pinnedJoint2 = createPinnedJoint2();
        beam = createBeamForInDeterminateProblem();

        Problem.Builder builder = new Problem.Builder();
        builder.addBeam(pinnedJoint1, beam);
        builder.addBeam(pinnedJoint2, beam);
        return builder.build();
    }

    private Beam createBeamForInDeterminateProblem() {
        ConcentratedForce concentratedForce = new ConcentratedForce(new Vector(0, 1, 0), new Vector(-2, 0, 0));
        Beam.Builder builder = new Beam.Builder("beam");
        builder.addConcentratedForce(concentratedForce);
        return builder.build();
    }

    private void thenResultHasIndeterminateStructuralDeterminacy() {
        assertThat(result.getStructureDeterminacy(), equalTo(StaticalDeterminacy.INDETERMINATE));
    }

    private void thenResultHasIndeterminateProblemDeterminacy() {
        assertThat(result.getProblemDeterminacy(), equalTo(StaticalDeterminacy.INDETERMINATE));
    }

    private void whenCalledWithDeterminateSimplySupportedBeamWithSupportsInLocalReferenceFrame() {
        Problem problem = createDeterminateSimplySupportedBeamWithSupportsInLocalReferenceFrame();
        result = solver.solve(problem);
    }

    private Problem createDeterminateSimplySupportedBeamWithSupportsInLocalReferenceFrame() {
        rollerJoint1 = createRollerJointInLocalReferenceFrame();
        pinnedJoint2 = createPinnedJointInLocalReferenceFrame();
        beam = createBeamForDeterminateProblem();

        Problem.Builder builder = new Problem.Builder();
        builder.addBeam(rollerJoint1, beam);
        builder.addBeam(pinnedJoint2, beam);
        return builder.build();
    }

    private Joint createRollerJointInLocalReferenceFrame() {
        Constraints hinge = new Constraints.Builder().constrainTx().constrainTy().build();
        Constraints rollerSupport = new Constraints.Builder().constrainTx().inReferenceFrame(new Vector(SQRT2DIV2, SQRT2DIV2, 0)).build();
        Joint.Builder builder = new Joint.Builder("roller joint 1", new Vector(0, 0, 0), hinge);
        builder.withSupport(rollerSupport);
        return builder.build();
    }

    private Joint createPinnedJointInLocalReferenceFrame() {
        Constraints hinge = new Constraints.Builder().constrainTx().constrainTy().build();
        Constraints pinnedSupport = new Constraints.Builder().constrainTx().constrainTy().inReferenceFrame(new Vector(SQRT2DIV2, -SQRT2DIV2,
                0)).build();
        Joint.Builder builder = new Joint.Builder("pinned joint 2", new Vector(0, 2, 0), hinge);
        builder.withSupport(pinnedSupport);
        return builder.build();
    }

    private void thenResultHasCorrectReactionsForDeterminateSimplySupportedBeamWithSupportsInLocalReferenceFrame() {
        Reaction expectedRollerSupportReaction = new Reaction(Ground.getId(), new ConcentratedForce(new Vector(0, 0, 0), new Vector(-1.5, -1.5,
                0)), null);
        Reaction expectedRollerFromBeamReaction = new Reaction(beam.getId(), new ConcentratedForce(new Vector(0, 0, 0), new Vector(1.5, 1.5, 0)),
                null);
        Reaction expectedPinnedSupportReaction = new Reaction(Ground.getId(), new ConcentratedForce(new Vector(0, 2, 0), new Vector(3.5, 3.5, 0)),
                null);
        Reaction expectedPinnedFromBeamReaction = new Reaction(beam.getId(), new ConcentratedForce(new Vector(0, 2, 0), new Vector(-3.5, -3.5, 0)),
                null);
        Reaction expectedBeamFromRollerReaction = new Reaction(rollerJoint1.getId(), new ConcentratedForce(new Vector(0, 0, 0), new Vector(-1.5,
                -1.5, 0)), null);
        Reaction expectedBeamFromPinnedReaction = new Reaction(pinnedJoint2.getId(), new ConcentratedForce(new Vector(0, 2, 0), new Vector(3.5,
                3.5, 0)), null);

        Map<String, Collection<Reaction>> reactions = result.getReactions();
        assertThat(reactions.keySet(), containsInAnyOrder(rollerJoint1.getId(), pinnedJoint2.getId(), beam.getId()));
        assertThat(reactions.get(rollerJoint1.getId()), containsInAnyOrder(expectedRollerSupportReaction, expectedRollerFromBeamReaction));
        assertThat(reactions.get(pinnedJoint2.getId()), containsInAnyOrder(expectedPinnedSupportReaction, expectedPinnedFromBeamReaction));
        assertThat(reactions.get(beam.getId()), containsInAnyOrder(expectedBeamFromRollerReaction, expectedBeamFromPinnedReaction));
    }

}
