package org.gabs.statics.solver.matrix.builder.processor;

import org.gabs.statics.solver.model.Structure;
import org.gabs.statics.solver.model.Effect;
import org.gabs.statics.solver.model.Element;
import org.gabs.statics.solver.registry.ConstraintRegistry;

import java.util.Collection;

/**
 * Interface for composing the equilibrium equations of a structural {@link Element} of a statics problem.
 */
public interface ElementProcessor {

    /**
     * Returns the row vectors representing the equilibrium equations of the processed structural element.
     * @param element the processed element
     * @param structure the structure
     * @param load load of the processed element
     * @param constraintRegistry registry of constraints, associating constraints to the column indices of unknowns representing them
     * @return the row vectors representing the equilibrium equations of the processed structural element
     */
    Collection<double[]> process(Element element, Structure structure, Collection<Effect> load, ConstraintRegistry constraintRegistry);

}
