package org.gabs.statics.solver.model;

import java.util.Collection;
import java.util.Map;

/**
 * Internal representation of a statics problem.
 * <p>
 * Consists of the following:
 * <ul><li>{@code structure}: The internal representation of the structure.</li>
 * <li>{@code load}: A mapping of structural elements and the load affecting them.</li>
 * <li>{@code precision}: Defines the precision of the result, e.g. the number of decimals in scalar values.</li></ul>
 */
public class Problem {

    private static final int DEFAULT_PRECISION = 12;

    private Structure structure;
    private Map<Element, Collection<Effect>> load;
    private int precision;

    /**
     * Constructs a new {@link Problem} instance.
     * @param structure a {@link Structure} representing the connections of structural elements
     * @param load a mapping of structural elements and the load affecting them
     * @param precision number of decimals, defines the precision of the result
     */
    public Problem(Structure structure, Map<Element, Collection<Effect>> load, Integer precision) {
        this.structure = structure;
        this.load = load;
        this.precision = precision != null ? precision : DEFAULT_PRECISION;
    }

    public Map<Element, Collection<Effect>> getLoad() {
        return load;
    }

    public Structure getStructure() {
        return structure;
    }

    public int getPrecision() {
        return precision;
    }

}
