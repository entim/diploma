package org.gabs.statics.solver.resultbuilder;

import org.gabs.statics.model.result.Reaction;
import org.gabs.statics.model.result.Result;
import org.gabs.statics.model.result.StaticalDeterminacy;
import org.gabs.statics.solver.converter.Converter;
import org.gabs.statics.solver.data.Analysis;
import org.gabs.statics.solver.model.Determinacy;
import org.gabs.statics.solver.model.Problem;
import org.gabs.statics.solver.registry.ConstraintRegistry;
import org.gabs.statics.solver.resultbuilder.postprocessor.ReactionMappingPostProcessor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collection;
import java.util.Map;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ResultBuilderImplTest {

    private static final int PRECISION = 1;
    private static final double[] SOLUTION = new double[]{1, 2};
    private static final String ERROR = "error";
    private static final Determinacy STRUCTURE_DETERMINACY = Determinacy.OVERDETERMINATE;
    private static final Determinacy PROBLEM_DETERMINACY = Determinacy.DETERMINATE;
    private static final StaticalDeterminacy CONVERTED_STRUCTURE_DETERMINACY = StaticalDeterminacy.OVERDETERMINATE;
    private static final StaticalDeterminacy CONVERTED_PROBLEM_DETERMINACY = StaticalDeterminacy.DETERMINATE;

    @InjectMocks private ResultBuilderImpl builder;
    @Mock private Converter<Determinacy, StaticalDeterminacy> determinacyConverter;
    @Mock private ReactionMappingBuilder reactionMappingBuilder;
    @Mock private ReactionMappingPostProcessor reactionMappingPostProcessor;
    @Mock private Analysis analysis;
    @Mock private Problem problem;
    @Mock private ConstraintRegistry constraintRegistry;
    @Mock private Map<String, Collection<Reaction>> reactionMapping;
    private Result result;

    @Test
    public void testBuildWithSolutionShouldReturnCorrectResult() {
        whenCalledWithSolution();
        thenResultShouldHaveCorrectReactionMapping();
        thenResultShouldHaveCorrectStructureDeterminacy();
        thenResultShouldHaveCorrectProblemDeterminacy();
        thenResultShouldHaveNullError();
    }

    @Test
    public void testBuildWithAnalysisShouldReturnCorrectAnalysis() {
        whenCalledWithAnalysis();
        thenResultShouldHaveNullReactionMapping();
        thenResultShouldHaveCorrectStructureDeterminacy();
        thenResultShouldHaveCorrectProblemDeterminacy();
        thenResultShouldHaveNullError();
    }

    @Test
    public void testBuildWithErrorShouldReturnError() {
        whenCalledWithError();
        thenResultShouldHaveError();
        thenResultShouldHaveNullReactionMapping();
        thenResultShouldHaveNullStructureDeterminacy();
        thenResultShouldHaveNullProblemDeterminacy();
    }

    private void whenCalledWithSolution() {
        when(analysis.getStructureDeterminacy()).thenReturn(STRUCTURE_DETERMINACY);
        when(analysis.getProblemDeterminacy()).thenReturn(PROBLEM_DETERMINACY);
        when(determinacyConverter.convert(STRUCTURE_DETERMINACY)).thenReturn(CONVERTED_STRUCTURE_DETERMINACY);
        when(determinacyConverter.convert(PROBLEM_DETERMINACY)).thenReturn(CONVERTED_PROBLEM_DETERMINACY);
        when(reactionMappingBuilder.build(SOLUTION, problem, constraintRegistry)).thenReturn(reactionMapping);
        when(problem.getPrecision()).thenReturn(PRECISION);
        result = builder.build(SOLUTION, analysis, problem, constraintRegistry);
    }

    private void thenResultShouldHaveCorrectReactionMapping() {
        verify(reactionMappingPostProcessor).postProcess(reactionMapping, PRECISION);
        assertThat(result.getReactions(), equalTo(reactionMapping));
    }

    private void thenResultShouldHaveCorrectStructureDeterminacy() {
        assertThat(result.getStructureDeterminacy(), equalTo(CONVERTED_STRUCTURE_DETERMINACY));
    }

    private void thenResultShouldHaveCorrectProblemDeterminacy() {
        assertThat(result.getProblemDeterminacy(), equalTo(CONVERTED_PROBLEM_DETERMINACY));
    }

    private void thenResultShouldHaveNullError() {
        assertThat(result.getError(), nullValue());
    }

    private void whenCalledWithAnalysis() {
        when(analysis.getStructureDeterminacy()).thenReturn(STRUCTURE_DETERMINACY);
        when(analysis.getProblemDeterminacy()).thenReturn(PROBLEM_DETERMINACY);
        when(determinacyConverter.convert(STRUCTURE_DETERMINACY)).thenReturn(CONVERTED_STRUCTURE_DETERMINACY);
        when(determinacyConverter.convert(PROBLEM_DETERMINACY)).thenReturn(CONVERTED_PROBLEM_DETERMINACY);
        result = builder.build(analysis);
    }

    private void thenResultShouldHaveNullReactionMapping() {
        assertThat(result.getReactions(), nullValue());
    }

    private void whenCalledWithError() {
        result = builder.build(ERROR);
    }

    private void thenResultShouldHaveError() {
        assertThat(result.getError(), equalTo(ERROR));
    }

    private void thenResultShouldHaveNullStructureDeterminacy() {
        assertThat(result.getStructureDeterminacy(), nullValue());
    }

    private void thenResultShouldHaveNullProblemDeterminacy() {
        assertThat(result.getProblemDeterminacy(), nullValue());
    }

}
