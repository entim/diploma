package org.gabs.statics.solver.registry;

import org.gabs.statics.solver.model.Constraint;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

public class ConstraintRegistryImplTest {

    private ConstraintRegistryImpl registry;
    private List<Constraint> constraints;
    private int sizeResult;

    @Before
    public void setup() {
        constraints = createTestConstraints();
    }

    @Test
    public void testRegisterWithDifferentConstraintsShouldResultIncreasingIndices() {
        givenConstraintRegistryImpl();
        whenRegisterIndexCalledWithTenDifferentConstraints();
        thenResultsShouldBeIncrementalZeroToNine();
    }

    @Test
    public void testGetIndexWithTheSameConstraintTwiceShouldReturnIdenticalIndices() {
        givenConstraintRegistryImpl();
        whenRegisterCalledWithTheSameConstraintTwice();
        thenIndexShouldBeZero();
    }

    @Test
    public void testSizeWithTenConstraintsShouldReturnTen() {
        givenConstraintRegistryImpl();
        whenRegisterIndexCalledWithTenDifferentConstraints();
        whenSizeCalled();
        thenResultShouldBeTen();
    }

    @Test
    public void testSizeWithTheSameConstraintsShouldReturnOne() {
        givenConstraintRegistryImpl();
        whenRegisterCalledWithTheSameConstraintTwice();
        whenSizeCalled();
        thenResultShouldBeOne();
    }

    private List<Constraint> createTestConstraints() {
        List<Constraint> result = new ArrayList<>(10);
        for (int i = 0; i < 10; i++) {
            result.add(mock(Constraint.class));
        }
        return result;
    }

    private void givenConstraintRegistryImpl() {
        registry = new ConstraintRegistryImpl();
    }

    private void whenRegisterIndexCalledWithTenDifferentConstraints() {
        for (Constraint constraint : constraints) {
            registry.register(constraint);
        }
    }

    private void thenResultsShouldBeIncrementalZeroToNine() {
        assertThat(registry.size(), equalTo(10));
        for (int i = 0; i < 10; i++) {
            Constraint constraint = constraints.get(i);
            assertThat(registry.getIndex(constraint), equalTo(i));
        }
    }

    private void whenRegisterCalledWithTheSameConstraintTwice() {
        Constraint constraint = constraints.get(0);
        registry.register(constraint);
        registry.register(constraint);
    }

    private void thenIndexShouldBeZero() {
        assertThat(registry.size(), equalTo(1));
        assertThat(registry.getIndex(constraints.get(0)), equalTo(0));
    }

    private void whenSizeCalled() {
        sizeResult = registry.size();
    }

    private void thenResultShouldBeTen() {
        assertThat(sizeResult, equalTo(10));
    }

    private void thenResultShouldBeOne() {
        assertThat(sizeResult, equalTo(1));
    }

}
