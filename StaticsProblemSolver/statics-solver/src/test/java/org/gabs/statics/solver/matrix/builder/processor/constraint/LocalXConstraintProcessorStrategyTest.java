package org.gabs.statics.solver.matrix.builder.processor.constraint;

import org.gabs.statics.solver.data.Vector;
import org.gabs.statics.solver.model.Constraint;
import org.gabs.statics.solver.model.Dof;
import org.gabs.statics.solver.model.ReferenceFrame;
import org.junit.Before;
import org.junit.Test;

import java.util.EnumMap;
import java.util.Map;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class LocalXConstraintProcessorStrategyTest {

    private static final int INDEX = 1;
    private static final double LOCAL_REFERENCE_FRAME_ROTATION = Math.PI / 6;
    private static final ReferenceFrame REFERENCE_FRAME = createLocalReferenceFrame();

    private LocalXConstraintProcessorStrategy strategy;
    private Map<Dof, double[]> coefficients;

    private static ReferenceFrame createLocalReferenceFrame() {
        double sin = Math.sin(LOCAL_REFERENCE_FRAME_ROTATION);
        double cos = Math.cos(LOCAL_REFERENCE_FRAME_ROTATION);
        Vector x = new Vector(cos, -sin, 0);
        Vector y = new Vector(sin, cos, 0);
        Vector z = new Vector(0, 0, 1);
        return new ReferenceFrame(x, y, z);
    }

    @Before
    public void setup() {
        coefficients = createBlankCoefficientsMap();
    }

    @Test
    public void testProcessWithJointConstrainedOnXShouldPutCosAsCoefficientAtIndex() {
        givenLocalXConstraintProcessorStrategy();
        whenCalledWithJointConstrainedOnXInLocalReferenceFrame();
        thenCoefficientShouldBeCosAtIndexInXCoefficients();
        thenCoefficientShouldBeZeroAtIndexInYCoefficients();
        thenCoefficientShouldBeZeroAtIndexInZZCoefficients();
    }

    @Test
    public void testProcessWithBeamConstrainedOnXShouldPutMinusCosAsCoefficientAtIndex() {
        givenLocalXConstraintProcessorStrategy();
        whenCalledWithBeamConstrainedOnXInLocalReferenceFrame();
        thenCoefficientShouldBeMinusCosAtIndexInXCoefficients();
        thenCoefficientShouldBeZeroAtIndexInYCoefficients();
        thenCoefficientShouldBeZeroAtIndexInZZCoefficients();
    }

    @Test
    public void testProcessWithJointConstrainedOnYShouldPutSinAsCoefficientAtIndex() {
        givenLocalXConstraintProcessorStrategy();
        whenCalledWithJointConstrainedOnYInLocalReferenceFrame();
        thenCoefficientShouldBeSinAtIndexInXCoefficients();
        thenCoefficientShouldBeZeroAtIndexInYCoefficients();
        thenCoefficientShouldBeZeroAtIndexInZZCoefficients();
    }

    @Test
    public void testProcessWithBeamConstrainedOnYShouldPutMinusSinAsCoefficientAtIndex() {
        givenLocalXConstraintProcessorStrategy();
        whenCalledWithBeamConstrainedOnYInLocalReferenceFrame();
        thenCoefficientShouldBeMinusSinAtIndexInXCoefficients();
        thenCoefficientShouldBeZeroAtIndexInYCoefficients();
        thenCoefficientShouldBeZeroAtIndexInZZCoefficients();
    }

    @Test
    public void testProcessWithJointConstrainedOnZZShouldNotPutCoefficientAtIndex() {
        givenLocalXConstraintProcessorStrategy();
        whenCalledWithJointConstrainedOnZZInLocalReferenceFrame();
        thenCoefficientShouldBeZeroAtIndexInXCoefficients();
        thenCoefficientShouldBeZeroAtIndexInYCoefficients();
        thenCoefficientShouldBeZeroAtIndexInZZCoefficients();
    }

    private Map<Dof, double[]> createBlankCoefficientsMap() {
        Map<Dof, double[]> result = new EnumMap<>(Dof.class);
        result.put(Dof.X, new double[]{0, 0, 0});
        result.put(Dof.Y, new double[]{0, 0, 0});
        result.put(Dof.ZZ, new double[]{0, 0, 0});
        return result;
    }

    private void givenLocalXConstraintProcessorStrategy() {
        strategy = new LocalXConstraintProcessorStrategy();
    }

    private void whenCalledWithJointConstrainedOnXInLocalReferenceFrame() {
        strategy.process(createConstraint(Dof.X), INDEX, true, coefficients);
    }

    private Constraint createConstraint(Dof constrainedDof) {
        return new Constraint(null, constrainedDof, REFERENCE_FRAME, null);
    }

    private void thenCoefficientShouldBeCosAtIndexInXCoefficients() {
        assertThat(coefficients.get(Dof.X)[INDEX], equalTo(Math.cos(LOCAL_REFERENCE_FRAME_ROTATION)));
    }

    private void thenCoefficientShouldBeZeroAtIndexInYCoefficients() {
        assertThat(coefficients.get(Dof.Y)[INDEX], equalTo(0d));
    }

    private void thenCoefficientShouldBeZeroAtIndexInZZCoefficients() {
        assertThat(coefficients.get(Dof.ZZ)[INDEX], equalTo(0d));
    }

    private void whenCalledWithBeamConstrainedOnXInLocalReferenceFrame() {
        strategy.process(createConstraint(Dof.X), INDEX, false, coefficients);
    }

    private void thenCoefficientShouldBeMinusCosAtIndexInXCoefficients() {
        assertThat(coefficients.get(Dof.X)[INDEX], equalTo(-Math.cos(LOCAL_REFERENCE_FRAME_ROTATION)));
    }

    private void whenCalledWithJointConstrainedOnYInLocalReferenceFrame() {
        strategy.process(createConstraint(Dof.Y), INDEX, true, coefficients);
    }

    private void thenCoefficientShouldBeSinAtIndexInXCoefficients() {
        assertThat(coefficients.get(Dof.X)[INDEX], equalTo(Math.sin(LOCAL_REFERENCE_FRAME_ROTATION)));
    }

    private void whenCalledWithBeamConstrainedOnYInLocalReferenceFrame() {
        strategy.process(createConstraint(Dof.Y), INDEX, false, coefficients);
    }

    private void thenCoefficientShouldBeMinusSinAtIndexInXCoefficients() {
        assertThat(coefficients.get(Dof.X)[INDEX], equalTo(-Math.sin(LOCAL_REFERENCE_FRAME_ROTATION)));
    }

    private void whenCalledWithJointConstrainedOnZZInLocalReferenceFrame() {
        strategy.process(createConstraint(Dof.ZZ), INDEX, true, coefficients);
    }

    private void thenCoefficientShouldBeZeroAtIndexInXCoefficients() {
        assertThat(coefficients.get(Dof.X)[INDEX], equalTo(0d));
    }

}
