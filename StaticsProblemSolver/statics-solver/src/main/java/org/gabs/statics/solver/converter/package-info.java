/**
 * Defines converter classes and interfaces to transform a {@code StaticsApi} problem to internal representation,
 * and an internal result to {@code StaticsApi} representation.
 */
package org.gabs.statics.solver.converter;