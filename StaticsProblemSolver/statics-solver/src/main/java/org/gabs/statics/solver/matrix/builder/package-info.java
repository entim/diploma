/**
 * Provides classes and interfaces to decompose a statics problem to a matrix representing the system of equilibrium equations.
 */
package org.gabs.statics.solver.matrix.builder;