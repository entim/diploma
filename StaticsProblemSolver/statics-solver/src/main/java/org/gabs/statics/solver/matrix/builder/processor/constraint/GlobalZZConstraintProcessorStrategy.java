package org.gabs.statics.solver.matrix.builder.processor.constraint;

import org.gabs.statics.solver.model.Constraint;
import org.gabs.statics.solver.model.Dof;

import java.util.Map;

/**
 * Implementation of the {@link ConstraintProcessorStrategy} interface that processes constraints on the direction of ZZ rotation, defined in global reference frame.
 */
class GlobalZZConstraintProcessorStrategy implements ConstraintProcessorStrategy {

    @Override
    public void process(Constraint constraint, int index, boolean joint, Map<Dof, double[]> coefficients) {
        if (Dof.X == constraint.getConstrainedDisplacement()) {
            addMomentFromX(constraint, index, joint, coefficients);
        } else if (Dof.Y == constraint.getConstrainedDisplacement()) {
            addMomentFromY(constraint, index, joint, coefficients);
        } else if (Dof.ZZ == constraint.getConstrainedDisplacement()) {
            addMomentFromZZ(index, joint, coefficients);
        }
    }

    private void addMomentFromX(Constraint constraint, int index, boolean joint, Map<Dof, double[]> coefficients) {
        double coefficient = joint ? 0 : constraint.getPosition().getY();
        coefficients.get(Dof.ZZ)[index] = coefficient;
    }

    private void addMomentFromY(Constraint constraint, int index, boolean joint, Map<Dof, double[]> coefficients) {
        double coefficient = joint ? 0 : -constraint.getPosition().getX();
        coefficients.get(Dof.ZZ)[index] = coefficient;
    }

    private void addMomentFromZZ(int index, boolean joint, Map<Dof, double[]> coefficients) {
        int coefficient = joint ? 1 : -1;
        coefficients.get(Dof.ZZ)[index] = coefficient;
    }

}
