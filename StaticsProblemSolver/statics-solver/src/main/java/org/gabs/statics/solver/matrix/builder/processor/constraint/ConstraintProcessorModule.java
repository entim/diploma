package org.gabs.statics.solver.matrix.builder.processor.constraint;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import java.util.Arrays;
import java.util.List;

public class ConstraintProcessorModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(ConstraintProcessor.class).to((ConstraintProcessorImpl.class));
    }

    @Provides
    @Singleton
    @Named("global")
    private List<ConstraintProcessorStrategy> provideGlobalConstraintProcessorStrategies() {
        return Arrays.asList(
                new GlobalXConstraintProcessorStrategy(),
                new GlobalYConstraintProcessorStrategy(),
                new GlobalZZConstraintProcessorStrategy());
    }

    @Provides
    @Singleton
    @Named("local")
    private List<ConstraintProcessorStrategy> provideLocalConstraintProcessorStrategies() {
        return Arrays.asList(
                new LocalXConstraintProcessorStrategy(),
                new LocalYConstraintProcessorStrategy(),
                new LocalZZConstraintProcessorStrategy());
    }

}
