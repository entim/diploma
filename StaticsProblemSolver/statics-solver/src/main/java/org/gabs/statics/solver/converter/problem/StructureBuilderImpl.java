package org.gabs.statics.solver.converter.problem;

import org.gabs.statics.model.problem.Beam;
import org.gabs.statics.model.problem.Joint;
import org.gabs.statics.model.problem.Problem;
import org.gabs.statics.solver.converter.Converter;
import org.gabs.statics.solver.data.Pair;
import org.gabs.statics.solver.model.Constraint;
import org.gabs.statics.solver.model.Element;
import org.gabs.statics.solver.model.Structure;

import javax.inject.Inject;
import java.util.Collection;
import java.util.Map;

/**
 * A {@link StructureBuilder} implementation.
 */
class StructureBuilderImpl implements StructureBuilder {

    private Converter<Pair<Joint, Beam>, Collection<Constraint>> constraintsConverter;

    @Override
    public Structure build(Problem problem, Map<org.gabs.statics.model.problem.Element, Element> elementMapping) {
        Structure result = new Structure();
        for (Joint joint : problem.getJoints()) {
            if (isSupported(joint)) {
                Collection<Constraint> constraints = constraintsConverter.convert(new Pair<Joint, Beam>(joint, null));
                result.addExternalConstraints(elementMapping.get(joint), constraints);
            }
            for (Beam beam : problem.getConnectedBeamsOf(joint.getId())) {
                Collection<Constraint> constraints = constraintsConverter.convert(new Pair<>(joint, beam));
                result.addInternalConstraints(elementMapping.get(joint), elementMapping.get(beam), constraints);
            }
        }
        return result;
    }

    private boolean isSupported(Joint joint) {
        return joint.getSupport() != null;
    }

    @Inject
    public void setConstraintsConverter(Converter<Pair<Joint, Beam>, Collection<Constraint>> constraintsConverter) {
        this.constraintsConverter = constraintsConverter;
    }

}
