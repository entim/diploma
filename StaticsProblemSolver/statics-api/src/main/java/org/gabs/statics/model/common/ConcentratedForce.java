package org.gabs.statics.model.common;

import java.util.Objects;

import static java.util.Objects.requireNonNull;

/**
 * Represents a concentrated force that has a point of application. Direction and magnitude of the force is described by the {@code vector} property.
 */
public class ConcentratedForce {

    private Vector pointOfApplication;
    private Vector vector;

    /**
     * Creates a new concentrated force with the given point of application and vector.
     * @param pointOfApplication the point of application
     * @param vector the force vector, describing the direction and magnitude of the force
     */
    public ConcentratedForce(Vector pointOfApplication, Vector vector) {
        this.pointOfApplication = requireNonNull(pointOfApplication, "Point of application must not be null.");
        this.vector = requireNonNull(vector, "Vector must not be null.");
    }

    @Override
    public int hashCode() {
        return Objects.hash(pointOfApplication, vector);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final ConcentratedForce other = (ConcentratedForce) obj;
        return Objects.equals(this.pointOfApplication, other.pointOfApplication) && Objects.equals(this.vector, other.vector);
    }

    @Override
    public String toString() {
        return "ConcentratedForce{" + "pointOfApplication=" + pointOfApplication + ", vector=" + vector + '}';
    }

    public Vector getVector() {
        return vector;
    }

    public Vector getPointOfApplication() {
        return pointOfApplication;
    }

}
