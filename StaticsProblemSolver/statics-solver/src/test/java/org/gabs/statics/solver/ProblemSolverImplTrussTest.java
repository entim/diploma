package org.gabs.statics.solver;

import org.gabs.statics.model.common.Vector;
import org.gabs.statics.model.problem.Beam;
import org.gabs.statics.model.common.ConcentratedForce;
import org.gabs.statics.model.problem.Constraints;
import org.gabs.statics.model.problem.Joint;
import org.gabs.statics.model.problem.Problem;
import org.gabs.statics.model.result.Ground;
import org.gabs.statics.model.result.Reaction;
import org.gabs.statics.model.result.Result;
import org.gabs.statics.model.result.StaticalDeterminacy;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collection;
import java.util.Map;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

public class ProblemSolverImplTrussTest {

    private static final int DEFAULT_PRECISION = 12;

    private static final double SIN_30 = Math.sin(Math.PI / 6);
    private static final double COS_30 = Math.cos(Math.PI / 6);
    private ProblemSolver solver;
    private Joint rollerJoint;
    private Joint pinnedJoint;
    private Joint hingeJoint1;
    private Joint hingeJoint2;
    private Joint hingeJoint3;
    private Joint hingeJoint4;
    private Joint hingeJoint5;
    private Joint hingeJoint6;
    private Beam beam1;
    private Beam beam2;
    private Beam beam3;
    private Beam beam4;
    private Beam beam5;
    private Beam beam6;
    private Beam beam7;
    private Beam beam8;
    private Beam beam9;
    private Beam beam10;
    private Beam beam11;
    private Beam beam12;
    private Beam beam13;
    private Result result;

    @Test
    public void testSolveHoweTruss() {
        givenProblemSolver();
        whenCalledWithHoweTruss();
        thenResultHasCorrectReactionsForHoweTruss();
        thenResultHasDeterminateStructuralDeterminacy();
        thenResultHasDeterminateProblemDeterminacy();
        thenResultHasNullError();
    }

    private void givenProblemSolver() {
        solver = new ProblemSolverFactoryImpl().create();
    }

    private void whenCalledWithHoweTruss() {
        Problem problem = createHoweTruss();
        result = solver.solve(problem);
    }

    private Problem createHoweTruss() {
        ConcentratedForce concentratedForce1 = new ConcentratedForce(new Vector(0, 1, 0), new Vector(-10, 0, 0));
        ConcentratedForce concentratedForce2 = new ConcentratedForce(new Vector(0, 2, 0), new Vector(-10, 0, 0));
        ConcentratedForce concentratedForce3 = new ConcentratedForce(new Vector(0, 3, 0), new Vector(-10, 0, 0));
        Constraints hinge = new Constraints.Builder().constrainTx().constrainTy().build();
        Constraints rollerSupport = new Constraints.Builder().constrainTx().build();
        Constraints pinnedSupport = new Constraints.Builder().constrainTx().constrainTy().build();
        pinnedJoint = new Joint.Builder("pinned joint", new Vector(0, 0, 0), hinge).withSupport(pinnedSupport).build();
        rollerJoint = new Joint.Builder("roller joint", new Vector(0, 4, 0), hinge).withSupport(rollerSupport).build();
        hingeJoint1 = new Joint.Builder("hinge joint 1", new Vector(0, 1, 0), hinge).addConcentratedForce(concentratedForce1).build();
        hingeJoint2 = new Joint.Builder("hinge joint 2", new Vector(0, 2, 0), hinge).addConcentratedForce(concentratedForce2).build();
        hingeJoint3 = new Joint.Builder("hinge joint 3", new Vector(0, 3, 0), hinge).addConcentratedForce(concentratedForce3).build();
        hingeJoint4 = new Joint.Builder("hinge joint 4", new Vector(2 * COS_30, 1, 0), hinge).build();
        hingeJoint5 = new Joint.Builder("hinge joint 5", new Vector(2 * COS_30, 2, 0), hinge).build();
        hingeJoint6 = new Joint.Builder("hinge joint 6", new Vector(2 * COS_30, 3, 0), hinge).build();
        beam1 = new Beam.Builder("beam 1").build();
        beam2 = new Beam.Builder("beam 2").build();
        beam3 = new Beam.Builder("beam 3").build();
        beam4 = new Beam.Builder("beam 4").build();
        beam5 = new Beam.Builder("beam 5").build();
        beam6 = new Beam.Builder("beam 6").build();
        beam7 = new Beam.Builder("beam 7").build();
        beam8 = new Beam.Builder("beam 8").build();
        beam9 = new Beam.Builder("beam 9").build();
        beam10 = new Beam.Builder("beam 10").build();
        beam11 = new Beam.Builder("beam 11").build();
        beam12 = new Beam.Builder("beam 12").build();
        beam13 = new Beam.Builder("beam 13").build();

        Problem.Builder builder = new Problem.Builder();
        builder.addBeams(pinnedJoint, beam1, beam5);
        builder.addBeams(hingeJoint1, beam1, beam2, beam6, beam7);
        builder.addBeams(hingeJoint2, beam2, beam3, beam8);
        builder.addBeams(hingeJoint3, beam3, beam4, beam9, beam10);
        builder.addBeams(hingeJoint4, beam5, beam6, beam12);
        builder.addBeams(hingeJoint5, beam7, beam8, beam9, beam12, beam13);
        builder.addBeams(hingeJoint6, beam10, beam11, beam13);
        builder.addBeams(rollerJoint, beam4, beam11);
        return builder.build();
    }

    private void thenResultHasCorrectReactionsForHoweTruss() {
        Reaction expectedPinnedSupportReaction = new Reaction(Ground.getId(), new ConcentratedForce(new Vector(0, 0, 0), new Vector(15, 0, 0)), null);
        Reaction expectedPinnedFromBeam1Reaction = new Reaction(beam1.getId(), new ConcentratedForce(new Vector(0, 0, 0), new Vector(0,
                round(15 * SIN_30 / COS_30, DEFAULT_PRECISION), 0)), null);
        Reaction expectedPinnedFromBeam5Reaction = new Reaction(beam5.getId(), new ConcentratedForce(new Vector(0, 0, 0),
                new Vector(-15, round(-15 * SIN_30 / COS_30, DEFAULT_PRECISION), 0)), null);
        Reaction expectedRollerSupportReaction = new Reaction(Ground.getId(), new ConcentratedForce(new Vector(0, 4, 0), new Vector(15, 0, 0)), null);
        Reaction expectedRollerFromBeam4Reaction = new Reaction(beam4.getId(), new ConcentratedForce(new Vector(0, 4, 0), new Vector(0,
                round(-15 * SIN_30 / COS_30, DEFAULT_PRECISION), 0)), null);
        Reaction expectedRollerFromBeam11Reaction = new Reaction(beam11.getId(), new ConcentratedForce(new Vector(0, 4, 0),
                new Vector(-15, round(15 * SIN_30 / COS_30, DEFAULT_PRECISION), 0)), null);
        Reaction expectedHinge1FromBeam1Reaction = new Reaction(beam1.getId(), new ConcentratedForce(new Vector(0, 1, 0), new Vector(0,
                round(-15 * SIN_30 / COS_30, DEFAULT_PRECISION), 0)), null);
        Reaction expectedHinge1FromBeam2Reaction = new Reaction(beam2.getId(), new ConcentratedForce(new Vector(0, 1, 0), new Vector(0,
                round(10 / COS_30, DEFAULT_PRECISION), 0)), null);
        Reaction expectedHinge1FromBeam6Reaction = new Reaction(beam6.getId(), new ConcentratedForce(new Vector(0, 1, 0), new Vector(15, 0, 0)),
                null);
        Reaction expectedHinge1FromBeam7Reaction = new Reaction(beam7.getId(), new ConcentratedForce(new Vector(0, 1, 0), new Vector(-5,
                round(-5 * SIN_30 / COS_30, DEFAULT_PRECISION), 0)), null);
        Reaction expectedHinge2FromBeam2Reaction = new Reaction(beam2.getId(), new ConcentratedForce(new Vector(0, 2, 0), new Vector(0,
                round(-10 / COS_30, DEFAULT_PRECISION), 0)), null);
        Reaction expectedHinge2FromBeam3Reaction = new Reaction(beam3.getId(), new ConcentratedForce(new Vector(0, 2, 0), new Vector(0,
                round(10 / COS_30, DEFAULT_PRECISION), 0)), null);
        Reaction expectedHinge2FromBeam8Reaction = new Reaction(beam8.getId(), new ConcentratedForce(new Vector(0, 2, 0), new Vector(10, 0, 0)),
                null);
        Reaction expectedHinge3FromBeam3Reaction = new Reaction(beam3.getId(), new ConcentratedForce(new Vector(0, 3, 0), new Vector(0,
                round(-10 / COS_30, DEFAULT_PRECISION), 0)), null);
        Reaction expectedHinge3FromBeam4Reaction = new Reaction(beam4.getId(), new ConcentratedForce(new Vector(0, 3, 0), new Vector(0,
                round(15 * SIN_30 / COS_30, DEFAULT_PRECISION), 0)), null);
        Reaction expectedHinge3FromBeam9Reaction = new Reaction(beam9.getId(), new ConcentratedForce(new Vector(0, 3, 0), new Vector(-5,
                round(5 * SIN_30 / COS_30, DEFAULT_PRECISION), 0)), null);
        Reaction expectedHinge3FromBeam10Reaction = new Reaction(beam10.getId(), new ConcentratedForce(new Vector(0, 3, 0), new Vector(15, 0, 0)),
                null);
        Reaction expectedHinge4FromBeam5Reaction = new Reaction(beam5.getId(), new ConcentratedForce(new Vector(2 * COS_30, 1, 0), new Vector(15,
                round(15 * SIN_30 / COS_30, DEFAULT_PRECISION), 0)), null);
        Reaction expectedHinge4FromBeam6Reaction = new Reaction(beam6.getId(), new ConcentratedForce(new Vector(2 * COS_30, 1, 0), new Vector(-15,
                0, 0)), null);
        Reaction expectedHinge4FromBeam12Reaction = new Reaction(beam12.getId(), new ConcentratedForce(new Vector(2 * COS_30, 1, 0), new Vector(0,
                round(-7.5 / COS_30, DEFAULT_PRECISION), 0)), null);
        Reaction expectedHinge5FromBeam7Reaction = new Reaction(beam7.getId(), new ConcentratedForce(new Vector(2 * COS_30, 2, 0), new Vector(5,
                round(5 * SIN_30 / COS_30, DEFAULT_PRECISION), 0)), null);
        Reaction expectedHinge5FromBeam8Reaction = new Reaction(beam8.getId(), new ConcentratedForce(new Vector(2 * COS_30, 2, 0), new Vector(-10,
                0, 0)), null);
        Reaction expectedHinge5FromBeam9Reaction = new Reaction(beam9.getId(), new ConcentratedForce(new Vector(2 * COS_30, 2, 0), new Vector(5,
                round(-5 * SIN_30 / COS_30, DEFAULT_PRECISION), 0)), null);
        Reaction expectedHinge5FromBeam12Reaction = new Reaction(beam12.getId(), new ConcentratedForce(new Vector(2 * COS_30, 2, 0), new Vector(0,
                round(7.5 / COS_30, DEFAULT_PRECISION), 0)), null);
        Reaction expectedHinge5FromBeam13Reaction = new Reaction(beam13.getId(), new ConcentratedForce(new Vector(2 * COS_30, 2, 0), new Vector(0,
                round(-7.5 / COS_30, DEFAULT_PRECISION), 0)), null);
        Reaction expectedHinge6FromBeam10Reaction = new Reaction(beam10.getId(), new ConcentratedForce(new Vector(2 * COS_30, 3, 0),
                new Vector(-15, 0, 0)), null);
        Reaction expectedHinge6FromBeam11Reaction = new Reaction(beam11.getId(), new ConcentratedForce(new Vector(2 * COS_30, 3, 0), new Vector(15,
                round(-15 * SIN_30 / COS_30, DEFAULT_PRECISION), 0)), null);
        Reaction expectedHinge6FromBeam13Reaction = new Reaction(beam13.getId(), new ConcentratedForce(new Vector(2 * COS_30, 3, 0), new Vector(0,
                round(7.5 / COS_30, DEFAULT_PRECISION), 0)), null);

        Reaction expectedBeam1FromPinnedReaction = new Reaction(pinnedJoint.getId(), new ConcentratedForce(new Vector(0, 0, 0), new Vector(0,
                round(-15 * SIN_30 / COS_30, DEFAULT_PRECISION), 0)), null);
        Reaction expectedBeam1FromHinge1Reaction = new Reaction(hingeJoint1.getId(), new ConcentratedForce(new Vector(0, 1, 0), new Vector(0,
                round(15 * SIN_30 / COS_30, DEFAULT_PRECISION), 0)), null);
        Reaction expectedBeam2FromHinge1Reaction = new Reaction(hingeJoint1.getId(), new ConcentratedForce(new Vector(0, 1, 0), new Vector(0,
                round(-10 / COS_30, DEFAULT_PRECISION), 0)), null);
        Reaction expectedBeam2FromHinge2Reaction = new Reaction(hingeJoint2.getId(), new ConcentratedForce(new Vector(0, 2, 0), new Vector(0,
                round(10 / COS_30, DEFAULT_PRECISION), 0)), null);
        Reaction expectedBeam3FromHinge2Reaction = new Reaction(hingeJoint2.getId(), new ConcentratedForce(new Vector(0, 2, 0), new Vector(0,
                round(-10 / COS_30, DEFAULT_PRECISION), 0)), null);
        Reaction expectedBeam3FromHinge3Reaction = new Reaction(hingeJoint3.getId(), new ConcentratedForce(new Vector(0, 3, 0), new Vector(0,
                round(10 / COS_30, DEFAULT_PRECISION), 0)), null);
        Reaction expectedBeam4FromHinge3Reaction = new Reaction(hingeJoint3.getId(), new ConcentratedForce(new Vector(0, 3, 0), new Vector(0,
                round(-15 * SIN_30 / COS_30, DEFAULT_PRECISION), 0)), null);
        Reaction expectedBeam4FromRollerReaction = new Reaction(rollerJoint.getId(), new ConcentratedForce(new Vector(0, 4, 0), new Vector(0,
                round(15 * SIN_30 / COS_30, DEFAULT_PRECISION), 0)), null);
        Reaction expectedBeam5FromPinnedReaction = new Reaction(pinnedJoint.getId(), new ConcentratedForce(new Vector(0, 0, 0), new Vector(15,
                round(15 * SIN_30 / COS_30, DEFAULT_PRECISION), 0)), null);
        Reaction expectedBeam5FromHinge4Reaction = new Reaction(hingeJoint4.getId(), new ConcentratedForce(new Vector(2 * COS_30, 1, 0),
                new Vector(-15, round(-15 * SIN_30 / COS_30, DEFAULT_PRECISION), 0)), null);
        Reaction expectedBeam6FromHinge1Reaction = new Reaction(hingeJoint1.getId(), new ConcentratedForce(new Vector(0, 1, 0), new Vector(-15, 0,
                0)), null);
        Reaction expectedBeam6FromHinge4Reaction = new Reaction(hingeJoint4.getId(), new ConcentratedForce(new Vector(2 * COS_30, 1, 0),
                new Vector(15, 0, 0)), null);
        Reaction expectedBeam7FromHinge1Reaction = new Reaction(hingeJoint1.getId(), new ConcentratedForce(new Vector(0, 1, 0), new Vector(5,
                round(5 * SIN_30 / COS_30, DEFAULT_PRECISION), 0)), null);
        Reaction expectedBeam7FromHinge5Reaction = new Reaction(hingeJoint5.getId(), new ConcentratedForce(new Vector(2 * COS_30, 2, 0),
                new Vector(-5, round(-5 * SIN_30 / COS_30, DEFAULT_PRECISION), 0)), null);
        Reaction expectedBeam8FromHinge2Reaction = new Reaction(hingeJoint2.getId(), new ConcentratedForce(new Vector(0, 2, 0), new Vector(-10, 0,
                0)), null);
        Reaction expectedBeam8FromHinge5Reaction = new Reaction(hingeJoint5.getId(), new ConcentratedForce(new Vector(2 * COS_30, 2, 0),
                new Vector(10, 0, 0)), null);
        Reaction expectedBeam9FromHinge3Reaction = new Reaction(hingeJoint3.getId(), new ConcentratedForce(new Vector(0, 3, 0), new Vector(5,
                round(-5 * SIN_30 / COS_30, DEFAULT_PRECISION), 0)), null);
        Reaction expectedBeam9FromHinge5Reaction = new Reaction(hingeJoint5.getId(), new ConcentratedForce(new Vector(2 * COS_30, 2, 0),
                new Vector(-5, round(5 * SIN_30 / COS_30, DEFAULT_PRECISION), 0)), null);
        Reaction expectedBeam10FromHinge3Reaction = new Reaction(hingeJoint3.getId(), new ConcentratedForce(new Vector(0, 3, 0), new Vector(-15, 0,
                0)), null);
        Reaction expectedBeam10FromHinge6Reaction = new Reaction(hingeJoint6.getId(), new ConcentratedForce(new Vector(2 * COS_30, 3, 0),
                new Vector(15, 0, 0)), null);
        Reaction expectedBeam11FromRollerReaction = new Reaction(rollerJoint.getId(), new ConcentratedForce(new Vector(0, 4, 0), new Vector(15,
                round(-15 * SIN_30 / COS_30, DEFAULT_PRECISION), 0)), null);
        Reaction expectedBeam11FromHinge6Reaction = new Reaction(hingeJoint6.getId(), new ConcentratedForce(new Vector(2 * COS_30, 3, 0),
                new Vector(-15, round(15 * SIN_30 / COS_30, DEFAULT_PRECISION), 0)), null);
        Reaction expectedBeam12FromHinge4Reaction = new Reaction(hingeJoint4.getId(), new ConcentratedForce(new Vector(2 * COS_30, 1, 0),
                new Vector(0, round(7.5 / COS_30, DEFAULT_PRECISION), 0)), null);
        Reaction expectedBeam12FromHinge5Reaction = new Reaction(hingeJoint5.getId(), new ConcentratedForce(new Vector(2 * COS_30, 2, 0),
                new Vector(0, round(-7.5 / COS_30, DEFAULT_PRECISION), 0)), null);
        Reaction expectedBeam13FromHinge5Reaction = new Reaction(hingeJoint5.getId(), new ConcentratedForce(new Vector(2 * COS_30, 2, 0),
                new Vector(0, round(7.5 / COS_30, DEFAULT_PRECISION), 0)), null);
        Reaction expectedBeam13FromHinge6Reaction = new Reaction(hingeJoint6.getId(), new ConcentratedForce(new Vector(2 * COS_30, 3, 0),
                new Vector(0, round(-7.5 / COS_30, DEFAULT_PRECISION), 0)), null);

        Map<String, Collection<Reaction>> reactions = result.getReactions();
        assertThat(reactions.keySet(), containsInAnyOrder(pinnedJoint.getId(), rollerJoint.getId(), hingeJoint1.getId(), hingeJoint2.getId(),
                hingeJoint3.getId(), hingeJoint4.getId(), hingeJoint5.getId(), hingeJoint6.getId(), beam1.getId(), beam2.getId(), beam3.getId(),
                beam4.getId(), beam5.getId(), beam6.getId(), beam7.getId(), beam8.getId(), beam9.getId(), beam10.getId(), beam11.getId(),
                beam12.getId(), beam13.getId()));
        assertThat(reactions.get(pinnedJoint.getId()), containsInAnyOrder(expectedPinnedSupportReaction, expectedPinnedFromBeam1Reaction,
                expectedPinnedFromBeam5Reaction));
        assertThat(reactions.get(rollerJoint.getId()), containsInAnyOrder(expectedRollerSupportReaction, expectedRollerFromBeam4Reaction,
                expectedRollerFromBeam11Reaction));
        assertThat(reactions.get(hingeJoint1.getId()), containsInAnyOrder(expectedHinge1FromBeam1Reaction, expectedHinge1FromBeam2Reaction,
                expectedHinge1FromBeam6Reaction, expectedHinge1FromBeam7Reaction));
        assertThat(reactions.get(hingeJoint2.getId()), containsInAnyOrder(expectedHinge2FromBeam2Reaction, expectedHinge2FromBeam3Reaction,
                expectedHinge2FromBeam8Reaction));
        assertThat(reactions.get(hingeJoint3.getId()), containsInAnyOrder(expectedHinge3FromBeam3Reaction, expectedHinge3FromBeam4Reaction,
                expectedHinge3FromBeam9Reaction, expectedHinge3FromBeam10Reaction));
        assertThat(reactions.get(hingeJoint4.getId()), containsInAnyOrder(expectedHinge4FromBeam5Reaction, expectedHinge4FromBeam6Reaction,
                expectedHinge4FromBeam12Reaction));
        assertThat(reactions.get(hingeJoint5.getId()), containsInAnyOrder(expectedHinge5FromBeam7Reaction, expectedHinge5FromBeam8Reaction,
                expectedHinge5FromBeam9Reaction, expectedHinge5FromBeam12Reaction, expectedHinge5FromBeam13Reaction));
        assertThat(reactions.get(hingeJoint6.getId()), containsInAnyOrder(expectedHinge6FromBeam10Reaction, expectedHinge6FromBeam11Reaction,
                expectedHinge6FromBeam13Reaction));

        assertThat(reactions.get(beam1.getId()), containsInAnyOrder(expectedBeam1FromPinnedReaction, expectedBeam1FromHinge1Reaction));
        assertThat(reactions.get(beam2.getId()), containsInAnyOrder(expectedBeam2FromHinge1Reaction, expectedBeam2FromHinge2Reaction));
        assertThat(reactions.get(beam3.getId()), containsInAnyOrder(expectedBeam3FromHinge2Reaction, expectedBeam3FromHinge3Reaction));
        assertThat(reactions.get(beam4.getId()), containsInAnyOrder(expectedBeam4FromHinge3Reaction, expectedBeam4FromRollerReaction));
        assertThat(reactions.get(beam5.getId()), containsInAnyOrder(expectedBeam5FromPinnedReaction, expectedBeam5FromHinge4Reaction));
        assertThat(reactions.get(beam6.getId()), containsInAnyOrder(expectedBeam6FromHinge1Reaction, expectedBeam6FromHinge4Reaction));
        assertThat(reactions.get(beam7.getId()), containsInAnyOrder(expectedBeam7FromHinge1Reaction, expectedBeam7FromHinge5Reaction));
        assertThat(reactions.get(beam8.getId()), containsInAnyOrder(expectedBeam8FromHinge2Reaction, expectedBeam8FromHinge5Reaction));
        assertThat(reactions.get(beam9.getId()), containsInAnyOrder(expectedBeam9FromHinge3Reaction, expectedBeam9FromHinge5Reaction));
        assertThat(reactions.get(beam10.getId()), containsInAnyOrder(expectedBeam10FromHinge3Reaction, expectedBeam10FromHinge6Reaction));
        assertThat(reactions.get(beam11.getId()), containsInAnyOrder(expectedBeam11FromRollerReaction, expectedBeam11FromHinge6Reaction));
        assertThat(reactions.get(beam12.getId()), containsInAnyOrder(expectedBeam12FromHinge4Reaction, expectedBeam12FromHinge5Reaction));
        assertThat(reactions.get(beam13.getId()), containsInAnyOrder(expectedBeam13FromHinge5Reaction, expectedBeam13FromHinge6Reaction));
    }

    private double round(double value, int precision) {
        return new BigDecimal(value).setScale(precision, RoundingMode.HALF_EVEN).doubleValue();
    }

    private void thenResultHasDeterminateStructuralDeterminacy() {
        assertThat(result.getStructureDeterminacy(), equalTo(StaticalDeterminacy.DETERMINATE));
    }

    private void thenResultHasDeterminateProblemDeterminacy() {
        assertThat(result.getProblemDeterminacy(), equalTo(StaticalDeterminacy.DETERMINATE));
    }

    private void thenResultHasNullError() {
        assertThat(result.getError(), nullValue());
    }

}
