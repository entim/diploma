package org.gabs.statics.solver.matrix.reducer;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;

import static org.junit.Assert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class GaussianEliminatorTest {

    private GaussianEliminator eliminator;
    private double[][] matrix;

    @Test
    public void testReduceWithSquareCoefficientMatrix() {
        givenGaussianEliminator();
        whenCalledWithSquareCoefficientMatrix();
        thenSquareCoefficientMatrixShouldBeCorrectlyReduced();
    }

    @Test
    public void testReduceWithSquareCoefficientMatrixShouldReorderRowsWhenPivoting() {
        givenGaussianEliminator();
        whenCalledWithSquareCoefficientMatrixWithLargestFirstElementNotInFirstRow();
        thenSquareCoefficientMatrixShouldHaveRowWithLargesFirstElementAtFirstIndex();
    }

    @Test
    public void testReduceWithCoefficientMatrixWiderThanHighShouldNotChangeMatrix() {
        givenGaussianEliminator();
        whenCalledWithCoefficientMatrixWiderThanHigh();
        thenMatrixShouldNotBeChanged();
    }

    @Test
    public void testReduceWithCoefficientMatrixHigherThanWideShouldResultZeroedRows() {
        givenGaussianEliminator();
        whenCalledWithCoefficientMatrixHigherThanWide();
        thenMatrixShouldBeCorrectlyReduced();
    }

    @Test(expected = ArithmeticException.class)
    public void testReduceWithSingulaMatrixShouldThrowException() {
        givenGaussianEliminator();
        whenCalledWithSingularMatrix();
    }

    private void givenGaussianEliminator() {
        eliminator = new GaussianEliminator();
    }

    private void whenCalledWithSquareCoefficientMatrix() {
        matrix = new double[][]{
                {3, 12, 9, 6},
                {1, 6, 1, 4},
                {1, 5, 4, 1}
        };
        eliminator.reduce(matrix);
    }

    private void thenSquareCoefficientMatrixShouldBeCorrectlyReduced() {
        assertThat(matrix[0], arrayItemsMatch(new double[]{3, 12, 9, 6}));
        assertThat(matrix[1], arrayItemsMatch(new double[]{0, 2, -2, 2}));
        assertThat(matrix[2], arrayItemsMatch(new double[]{0, 0, 2, -2}));
    }

    private void whenCalledWithSquareCoefficientMatrixWithLargestFirstElementNotInFirstRow() {
        matrix = new double[][]{
                {1, 6, 1, 4},
                {3, 12, 9, 6},
                {1, 5, 4, 1}
        };
        eliminator.reduce(matrix);
    }

    private void thenSquareCoefficientMatrixShouldHaveRowWithLargesFirstElementAtFirstIndex() {
        assertThat(matrix[0], arrayItemsMatch(new double[]{3, 12, 9, 6}));
    }

    private void whenCalledWithCoefficientMatrixWiderThanHigh() {
        matrix = new double[][]{
                {1, 6, 1, 4},
                {3, 12, 9, 6}
        };
        eliminator.reduce(matrix);
    }

    private void thenMatrixShouldNotBeChanged() {
        assertThat(matrix[0], arrayItemsMatch(new double[]{1, 6, 1, 4}));
        assertThat(matrix[1], arrayItemsMatch(new double[]{3, 12, 9, 6}));
    }

    private void whenCalledWithCoefficientMatrixHigherThanWide() {
        matrix = new double[][]{
                {3, 12, 9, 6},
                {1, 6, 1, 4},
                {1, 5, 4, 1},
                {3, 11, 9, 8}
        };
        eliminator.reduce(matrix);
    }

    private void thenMatrixShouldBeCorrectlyReduced() {
        assertThat(matrix[0], arrayItemsMatch(new double[]{3, 12, 9, 6}));
        assertThat(matrix[1], arrayItemsMatch(new double[]{0, 2, -2, 2}));
        assertThat(matrix[2], arrayItemsMatch(new double[]{0, 0, 2, -2}));
        assertThat(matrix[3], arrayItemsMatch(new double[]{0, 0, 0, 2}));
    }

    private void whenCalledWithSingularMatrix() {
        matrix = new double[][]{
                {1, 0, 0},
                {1, 0, 0},
                {1, 0, 0}
        };
        eliminator.reduce(matrix);
    }

    private Matcher<double[]> arrayItemsMatch(final double[] element) {
        return new TypeSafeMatcher<double[]>() {
            @Override
            protected boolean matchesSafely(double[] item) {
                boolean result = true;
                for (int i = 0; i < element.length; i++) {
                    if (element[i] != item[i]) {
                        result = false;
                        break;
                    }
                }
                return result;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("Array items should match ").appendValue(Arrays.toString(element));
            }
        };
    }

}
