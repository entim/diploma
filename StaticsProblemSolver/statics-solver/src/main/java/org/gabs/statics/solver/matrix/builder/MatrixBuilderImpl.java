package org.gabs.statics.solver.matrix.builder;

import org.gabs.statics.solver.matrix.builder.processor.ElementProcessor;
import org.gabs.statics.solver.model.Effect;
import org.gabs.statics.solver.model.Element;
import org.gabs.statics.solver.model.Problem;
import org.gabs.statics.solver.registry.ConstraintRegistry;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collection;

/**
 * An implementation of the {@link MatrixBuilder} interface that uses {@link ElementProcessor}s to decompose a statics problem to equilibrium equations.
 */
class MatrixBuilderImpl implements MatrixBuilder {

    private ElementProcessor elementProcessor;

    @Override
    public double[][] build(Problem problem, ConstraintRegistry constraintRegistry) {
        Collection<double[]> coefficients = new ArrayList<>();
        for (Element element : problem.getStructure().getElements()) {
            Collection<Effect> load = problem.getLoad().get(element);
            coefficients.addAll(elementProcessor.process(element, problem.getStructure(), load, constraintRegistry));
        }
        return coefficients.toArray(new double[coefficients.size()][]);
    }

    @Inject
    public void setElementProcessor(ElementProcessor elementProcessor) {
        this.elementProcessor = elementProcessor;
    }

}
