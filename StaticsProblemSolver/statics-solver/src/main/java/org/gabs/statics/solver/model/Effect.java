package org.gabs.statics.solver.model;

import org.gabs.statics.solver.data.Vector;

/**
 * Represents an effect (force or moment) on a structural element.
 */
public interface Effect {

    /**
     * Returns the resultant of the effect.
     * @return the resultant of the effect, defining the direction and magnitude
     */
    Vector getResultant();

}
