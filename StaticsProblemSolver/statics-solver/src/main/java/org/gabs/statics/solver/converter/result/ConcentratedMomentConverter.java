package org.gabs.statics.solver.converter.result;

import org.gabs.statics.model.common.Vector;
import org.gabs.statics.model.common.Moment;
import org.gabs.statics.solver.converter.Converter;
import org.gabs.statics.solver.data.Pair;
import org.gabs.statics.solver.model.Constraint;

/**
 * A {@link Converter} implementation that converts an internal representation of load to a {@code StaticsApi} {@link Moment}.
 */
class ConcentratedMomentConverter implements Converter<Pair<Constraint, Double>, Moment> {

    @Override
    public Moment convert(Pair<Constraint, Double> source) {
        return new Moment(new Vector(0, 0, source.getRight()));
    }

}
