package org.gabs.statics.solver.model;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Represents a structure. The structure is modeled as a table that uses the structural elements as column and row keys,
 * and the constraints between elements as table cells. External constraints are added in the cell of the supported element and
 * {@link Element#GROUND}.
 */
public class Structure {

    private Map<Element, Map<Element, Collection<Constraint>>> backingMap = new HashMap<>();

    /**
     * Adds an internal constraint to the structure. The constraint connects the given elements.
     * @param element1 a structural element
     * @param element2 a structural element
     * @param constraints constraints connecting the given elements
     */
    public void addInternalConstraints(Element element1, Element element2, Collection<Constraint> constraints) {
        rowMap(element1).put(element2, constraints);
        rowMap(element2).put(element1, constraints);
    }

    /**
     * Add an external constraint to th structure. The constraint connects the given element to the ground.
     * @param element a structural element
     * @param constraints constraints connecting the given element to the ground
     */
    public void addExternalConstraints(Element element, Collection<Constraint> constraints) {
        rowMap(element).put(Element.GROUND, constraints);
    }

    /**
     * Returns the constraints connecting the given two elements.
     * @param element1 a structural element
     * @param element2 a structural element
     * @return the constraints connecting the given two elements
     */
    public Collection<Constraint> getConstraintsBetween(Element element1, Element element2) {
        return rowMap(element1).get(element2);
    }

    /**
     * Returns the constraints of the given element.
     * @param element a structural element
     * @return the constraints of the given element
     */
    public Collection<Collection<Constraint>> getConstraintsOf(Element element) {
        return rowMap(element).values();
    }

    /**
     * Returns the structural elements.
     * @return the structural elements
     */
    public Collection<Element> getElements() {
        return backingMap.keySet();
    }

    /**
     * Returns the elements connected to the given element.
     * @param element a structural element
     * @return the elements connected to the given element
     */
    public Collection<Element> getConnectedElementsOf(Element element) {
        return rowMap(element).keySet();
    }

    private Map<Element, Collection<Constraint>> rowMap(Element rowKey) {
        Map<Element, Collection<Constraint>> row = backingMap.get(rowKey);
        if (row == null) {
            row = new HashMap<>();
            backingMap.put(rowKey, row);
        }
        return row;
    }

}
