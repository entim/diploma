package org.gabs.statics.solver.resultbuilder;

import org.gabs.statics.model.result.Result;
import org.gabs.statics.solver.data.Analysis;
import org.gabs.statics.solver.model.Problem;
import org.gabs.statics.solver.registry.ConstraintRegistry;

/**
 * Interface for building the result of a statics problem.
 * A result consists of:
 * <ul><li>reactions and the results of determinacy analysis when the problem is solvable</li>
 * <li>the results of determinacy analysis when the problem is not solvable</li>
 * <li>an error message when the problem is invalid.</li></ul>
 */
public interface ResultBuilder {

    /**
     * Builds the result of a solvable statics problem.
     * @param solution the vector containing the solution of the equilibrium equations
     * @param analysis result of the determinacy analysis
     * @param problem the statics problem
     * @param constraintRegistry registry of constraints, associating constraints to the column indices of unknowns representing them
     * @return the result containing reactions and the result of determinacy analysis
     */
    Result build(double[] solution, Analysis analysis, Problem problem, ConstraintRegistry constraintRegistry);

    /**
     * Builds the result of statics problem that is not solvable.
     * @param analysis result of the determinacy analysis
     * @return the result containing the result of determinacy analysis
     */
    Result build(Analysis analysis);

    /**
     * Builds the result of an invalid statics problem.
     * @param error an error message stating the reason of invalidating the problem
     * @return the result containing an error message
     */
    Result build(String error);

}
