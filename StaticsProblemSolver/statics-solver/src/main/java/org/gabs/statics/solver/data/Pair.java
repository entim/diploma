package org.gabs.statics.solver.data;

import java.util.Objects;

/**
 * Associates two values.
 * @param <L> left value type
 * @param <R> right value type
 */
public class Pair<L, R> {

    private L left;
    private R right;

    /**
     * Constructs a {@link Pair} instance with the given values.
     * @param left left value
     * @param right right value
     */
    public Pair(L left, R right) {
        this.left = left;
        this.right = right;
    }

    @Override
    public int hashCode() {
        return Objects.hash(left, right);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final Pair other = (Pair) obj;
        return Objects.equals(this.left, other.left) && Objects.equals(this.right, other.right);
    }

    @Override
    public String toString() {
        return "Pair{" + "left=" + left + ", right=" + right + '}';
    }

    public L getLeft() {
        return left;
    }

    public R getRight() {
        return right;
    }

}
