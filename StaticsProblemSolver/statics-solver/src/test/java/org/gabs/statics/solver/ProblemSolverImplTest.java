package org.gabs.statics.solver;

import org.gabs.statics.model.problem.Problem;
import org.gabs.statics.model.result.Result;
import org.gabs.statics.solver.analyzer.ProblemAnalyzer;
import org.gabs.statics.solver.converter.Converter;
import org.gabs.statics.solver.data.Analysis;
import org.gabs.statics.solver.model.Structure;
import org.gabs.statics.solver.matrix.builder.MatrixBuilder;
import org.gabs.statics.solver.matrix.extractor.SolutionExtractor;
import org.gabs.statics.solver.matrix.reducer.RowReducer;
import org.gabs.statics.solver.model.Determinacy;
import org.gabs.statics.solver.registry.ConstraintRegistry;
import org.gabs.statics.solver.registry.ConstraintRegistryFactory;
import org.gabs.statics.solver.resultbuilder.ResultBuilder;
import org.gabs.statics.solver.data.Error;
import org.gabs.statics.solver.validator.ProblemValidator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ProblemSolverImplTest {

    private static final String ERROR = "error";

    @InjectMocks private ProblemSolverImpl solver;
    @Mock private ConstraintRegistryFactory constraintRegistryFactory;
    @Mock private ProblemValidator problemValidator;
    @Mock private Converter<Problem, org.gabs.statics.solver.model.Problem> problemConverter;
    @Mock private MatrixBuilder matrixBuilder;
    @Mock private RowReducer rowReducer;
    @Mock private ProblemAnalyzer problemAnalyzer;
    @Mock private SolutionExtractor solutionExtractor;
    @Mock private ResultBuilder resultBuilder;
    @Mock private Problem problem;
    @Mock private org.gabs.statics.solver.model.Problem internalProblem;
    @Mock private Structure structure;
    @Mock private Result builtResult;
    @Mock private Error error;
    private Result result;

    @Test
    public void testSolveWithValidDeterminateProblemShouldReturnCorrectResult() {
        whenCalledWithValidDeterminateProblem();
        thenResultIsReturnCorrectResultForValidDeterminateProblem();
    }

    @Test
    public void testSolveWithValidIndeterminateProblemShouldReturnAnalysis() {
        whenCalledWithValidIndeterminateProblem();
        thenResultBuilderIsCalledWithAnalysis();
    }

    @Test
    public void testSolveWithValidOverdeterminateProblemShouldReturnAnalysis() {
        whenCalledWithValidOverdeterminateProblem();
        thenResultBuilderIsCalledWithAnalysis();
    }

    @Test
    public void testSolveWithInvalidProblemShouldReturnError() {
        whenCalledWithInvalidProblem();
        thenResultBuilderIsCalledWithError();
    }

    private void whenCalledWithValidDeterminateProblem() {
        when(problemValidator.validate(problem)).thenReturn(null);
        when(problemConverter.convert(problem)).thenReturn(internalProblem);
        when(internalProblem.getStructure()).thenReturn(structure);
        when(problemAnalyzer.analyze(any(double[][].class))).thenReturn(new Analysis(Determinacy.DETERMINATE, Determinacy.DETERMINATE));
        when(resultBuilder.build(any(double[].class), any(Analysis.class), eq(internalProblem), any(ConstraintRegistry.class))).thenReturn
                (builtResult);
        result = solver.solve(problem);
    }

    private void thenResultIsReturnCorrectResultForValidDeterminateProblem() {
        assertThat(result, equalTo(builtResult));
    }

    private void whenCalledWithValidIndeterminateProblem() {
        when(problemValidator.validate(problem)).thenReturn(null);
        when(problemConverter.convert(problem)).thenReturn(internalProblem);
        when(internalProblem.getStructure()).thenReturn(structure);
        when(problemAnalyzer.analyze(any(double[][].class))).thenReturn(new Analysis(Determinacy.INDETERMINATE, Determinacy.INDETERMINATE));
        result = solver.solve(problem);
    }

    private void thenResultBuilderIsCalledWithAnalysis() {
        verify(resultBuilder).build(any(Analysis.class));
    }

    private void whenCalledWithValidOverdeterminateProblem() {
        when(problemValidator.validate(problem)).thenReturn(null);
        when(problemConverter.convert(problem)).thenReturn(internalProblem);
        when(internalProblem.getStructure()).thenReturn(structure);
        when(problemAnalyzer.analyze(any(double[][].class))).thenReturn(new Analysis(Determinacy.OVERDETERMINATE, Determinacy.OVERDETERMINATE));
        result = solver.solve(problem);
    }

    private void whenCalledWithInvalidProblem() {
        when(problemValidator.validate(problem)).thenReturn(error);
        when(error.getDescription()).thenReturn(ERROR);
        result = solver.solve(problem);
    }

    private void thenResultBuilderIsCalledWithError() {
        verify(resultBuilder).build(ERROR);
    }

}
