package org.gabs.statics.solver.resultbuilder;

import org.gabs.statics.model.result.Reaction;
import org.gabs.statics.solver.model.Problem;
import org.gabs.statics.solver.registry.ConstraintRegistry;

import java.util.Collection;
import java.util.Map;

/**
 * Interface for building a reaction mapping that associates reactions to structural elements.
 */
interface ReactionMappingBuilder {

    /**
     * Assembles the reactions map that associates reactions to structural element IDs.
     * @param solution the vector containing the solution of the equilibrium equations
     * @param problem the statics problem
     * @param constraintRegistry registry of constraints, associating constraints to the column indices of unknowns representing them
     * @return the reaction mapping
     */
    Map<String, Collection<Reaction>> build(double[] solution, Problem problem, ConstraintRegistry constraintRegistry);

}
