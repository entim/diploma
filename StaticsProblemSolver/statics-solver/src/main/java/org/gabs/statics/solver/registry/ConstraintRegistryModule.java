package org.gabs.statics.solver.registry;

import com.google.inject.AbstractModule;

public class ConstraintRegistryModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(ConstraintRegistry.class).to(ConstraintRegistryImpl.class);
        bind(ConstraintRegistryFactory.class).to(ConstraintRegistryFactoryImpl.class);
    }

}
