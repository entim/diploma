package org.gabs.statics.solver.converter.result;

import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import org.gabs.statics.model.common.Vector;
import org.gabs.statics.model.common.ConcentratedForce;
import org.gabs.statics.model.common.Moment;
import org.gabs.statics.model.result.StaticalDeterminacy;
import org.gabs.statics.solver.converter.Converter;
import org.gabs.statics.solver.data.Pair;
import org.gabs.statics.solver.model.Constraint;
import org.gabs.statics.solver.model.Determinacy;

public class ResultConverterModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(new TypeLiteral<Converter<Pair<Constraint, Double>, ConcentratedForce>>() {}).to(org.gabs.statics.solver.converter.result
                .ConcentratedForceConverter.class);
        bind(new TypeLiteral<Converter<Pair<Constraint, Double>, Moment>>() {}).to(org.gabs.statics.solver.converter.result
                .ConcentratedMomentConverter.class);
        bind(new TypeLiteral<Converter<Determinacy, StaticalDeterminacy>>() {}).to(DeterminacyConverter.class);
        bind(new TypeLiteral<Converter<org.gabs.statics.solver.data.Vector, Vector>>() {}).to(org.gabs.statics.solver.converter.result
                .VectorConverter.class);
    }

}
