package org.gabs.statics.solver.matrix.builder.processor.load;

import org.gabs.statics.solver.data.Vector;
import org.gabs.statics.solver.model.ConcentratedForce;
import org.gabs.statics.solver.model.ConcentratedMoment;
import org.gabs.statics.solver.model.Dof;
import org.gabs.statics.solver.model.Effect;
import org.junit.Before;
import org.junit.Test;

import java.util.EnumMap;
import java.util.Map;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class MomentProcessorStrategyTest {

    private static final double INITIAL_CONSTANT = 1;
    private static final Vector MOMENT_VECTOR = new Vector(0, 0, 1);
    private static final Effect CONCENTRATED_MOMENT = new ConcentratedMoment(MOMENT_VECTOR);

    private MomentProcessorStrategy strategy;
    private Map<Dof, double[]> coefficients;

    @Before
    public void setup() {
        coefficients = createCoefficientsMap();
    }

    @Test
    public void testProcessWithConcentratedMomentShouldAddNegatedMomentToLastIndexOfZZCoefficients() {
        givenConcentratedMomentProcessorStrategy();
        whenCalledWithConcentratedMoment();
        thenNegatedMomentShouldBeAddedToLastIndexOfZZCoefficients();
    }

    @Test
    public void testProcessWithConcentratedForceShouldNotChangeCoefficients() {
        givenConcentratedMomentProcessorStrategy();
        whenCalledWithConcentratedForce();
        thenCoefficientsShouldHaveInitialConstantsAtLastIndex();
    }

    private Map<Dof, double[]> createCoefficientsMap() {
        Map<Dof, double[]> result = new EnumMap<>(Dof.class);
        result.put(Dof.X, new double[]{0, 0, INITIAL_CONSTANT});
        result.put(Dof.Y, new double[]{0, 0, INITIAL_CONSTANT});
        result.put(Dof.ZZ, new double[]{0, 0, INITIAL_CONSTANT});
        return result;
    }

    private void givenConcentratedMomentProcessorStrategy() {
        strategy = new MomentProcessorStrategy();
    }

    private void whenCalledWithConcentratedMoment() {
        strategy.process(CONCENTRATED_MOMENT, coefficients, true);
    }

    private void thenNegatedMomentShouldBeAddedToLastIndexOfZZCoefficients() {
        assertThat(coefficients.get(Dof.ZZ)[2], equalTo(INITIAL_CONSTANT - MOMENT_VECTOR.getZ()));
    }

    private void whenCalledWithConcentratedForce() {
        strategy.process(new ConcentratedForce(new Vector(1, 0, 0), new Vector(1, 0, 0)), coefficients, true);
    }

    private void thenCoefficientsShouldHaveInitialConstantsAtLastIndex() {
        assertThat(coefficients.get(Dof.X)[2], equalTo(INITIAL_CONSTANT));
        assertThat(coefficients.get(Dof.Y)[2], equalTo(INITIAL_CONSTANT));
        assertThat(coefficients.get(Dof.ZZ)[2], equalTo(INITIAL_CONSTANT));
    }

}
