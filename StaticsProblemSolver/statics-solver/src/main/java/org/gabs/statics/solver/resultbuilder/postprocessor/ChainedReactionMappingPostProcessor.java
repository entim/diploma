package org.gabs.statics.solver.resultbuilder.postprocessor;

import org.gabs.statics.model.result.Reaction;

import javax.inject.Inject;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Implementation of the {@link ReactionMappingPostProcessor} interface that executes a list of post-processors.
 */
class ChainedReactionMappingPostProcessor implements ReactionMappingPostProcessor {

    private List<ReactionMappingPostProcessor> postProcessors;

    @Override
    public void postProcess(Map<String, Collection<Reaction>> reactions, int precision) {
        for (ReactionMappingPostProcessor postProcessor : postProcessors) {
            postProcessor.postProcess(reactions, precision);
        }
    }

    @Inject
    public void setPostProcessors(List<ReactionMappingPostProcessor> postProcessors) {
        this.postProcessors = postProcessors;
    }

}
