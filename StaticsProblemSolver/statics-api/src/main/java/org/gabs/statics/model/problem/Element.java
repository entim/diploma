package org.gabs.statics.model.problem;

import org.gabs.statics.model.common.ConcentratedForce;
import org.gabs.statics.model.common.Moment;

import java.util.Set;

/**
 * Represents a structural element.
 * Element ID is unique within a structure.
 */
public interface Element {

    /**
     * Returns the element ID.
     * @return a unique ID
     */
    String getId();

    /**
     * Returns concentrated forces on this element.
     * @return a set of concentrated forces
     */
    Set<ConcentratedForce> getConcentratedForces();


    /**
     * Returns concentrated moments on this element.
     * @return a set of concentrated moments
     */
    Set<Moment> getMoments();

}
