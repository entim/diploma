/**
 * Defines classes and interfaces to build the result of a statics problem.
 */
package org.gabs.statics.solver.resultbuilder;