package org.gabs.statics.solver.resultbuilder.postprocessor;

import org.gabs.statics.model.problem.Element;
import org.gabs.statics.model.result.Reaction;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class ChainedReactionMappingPostProcessorTest {

    private static final int PRECISION = 14;

    private ChainedReactionMappingPostProcessor postProcessor;
    private Map<String, Collection<Reaction>> reactions;
    @Mock private Element element;
    @Mock private Reaction reaction;
    @Mock private ReactionMappingPostProcessor postProcessor1;
    @Mock private ReactionMappingPostProcessor postProcessor2;

    @Test
    public void testPostProcessWithReactionMappingShouldCallEveryPostProcessor() {
        givenChainedReactionMappingPostProcessor();
        whenCalledWithReactionMapping();
        thenEveryPostProcessorShouldBeCalled();
    }

    private void givenChainedReactionMappingPostProcessor() {
        postProcessor = new ChainedReactionMappingPostProcessor();
        postProcessor.setPostProcessors(Arrays.asList(postProcessor1, postProcessor2));
    }

    private void whenCalledWithReactionMapping() {
        reactions = Collections.<String, Collection<Reaction>>singletonMap(element.getId(), Collections.singleton(reaction));
        postProcessor.postProcess(reactions, PRECISION);
    }

    private void thenEveryPostProcessorShouldBeCalled() {
        verify(postProcessor1).postProcess(reactions, PRECISION);
        verify(postProcessor2).postProcess(reactions, PRECISION);
    }

}
