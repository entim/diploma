package org.gabs.statics.solver.converter.problem;

import org.gabs.statics.model.common.Vector;
import org.gabs.statics.solver.converter.Converter;
import org.gabs.statics.solver.model.ConcentratedForce;
import org.gabs.statics.solver.model.Effect;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ConcentratedForceConverterTest {

    private static final Vector POINT_OF_APPLICATION = mock(Vector.class);
    private static final Vector FORCE_VECTOR = mock(Vector.class);
    private static final org.gabs.statics.solver.data.Vector CONVERTED_POINT_OF_APPLICATION = mock(org.gabs.statics.solver.data.Vector.class);
    private static final org.gabs.statics.solver.data.Vector CONVERTED_FORCE_VECTOR = mock(org.gabs.statics.solver.data.Vector.class);

    @InjectMocks private ConcentratedForceConverter converter;
    @Mock private Converter<Vector, org.gabs.statics.solver.data.Vector> vectorConverter;
    @Mock private org.gabs.statics.model.common.ConcentratedForce force;
    private Effect result;

    @Test
    public void testConvertResultShouldBeAConcentratedForceInstance() {
        whenCalledWithConcentratedForce();
        thenResultShouldBeAConcentratedForceInstance();
    }

    @Test
    public void testConvertForceShouldReturnCorrectPointOfApplication() {
        whenCalledWithConcentratedForce();
        thenResultShouldHaveCorrectPointOfApplication();
    }

    @Test
    public void testConvertForceShouldReturnCorrectVector() {
        whenCalledWithConcentratedForce();
        thenResultShouldHaveCorrectVector();
    }

    private void whenCalledWithConcentratedForce() {
        when(force.getPointOfApplication()).thenReturn(POINT_OF_APPLICATION);
        when(force.getVector()).thenReturn(FORCE_VECTOR);
        when(vectorConverter.convert(POINT_OF_APPLICATION)).thenReturn(CONVERTED_POINT_OF_APPLICATION);
        when(vectorConverter.convert(FORCE_VECTOR)).thenReturn(CONVERTED_FORCE_VECTOR);
        result = converter.convert(force);
    }

    private void thenResultShouldBeAConcentratedForceInstance() {
        assertThat(result, instanceOf(ConcentratedForce.class));
    }

    private void thenResultShouldHaveCorrectPointOfApplication() {
        ConcentratedForce resultForce = (ConcentratedForce) result;
        assertThat(resultForce.getPointOfApplication(), equalTo(CONVERTED_POINT_OF_APPLICATION));
    }

    private void thenResultShouldHaveCorrectVector() {
        ConcentratedForce resultForce = (ConcentratedForce) result;
        assertThat(resultForce.getResultant(), equalTo(CONVERTED_FORCE_VECTOR));

    }

}
