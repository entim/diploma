package org.gabs.statics.model.problem;

import org.gabs.statics.model.common.ConcentratedForce;
import org.gabs.statics.model.common.Moment;
import org.gabs.statics.model.common.Vector;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static java.util.Objects.requireNonNull;

/**
 * Represents a joint, connecting, and supporting beams.
 * Use {@link Joint.Builder} to instantiate.
 */
public class Joint implements Element {

    private String id;
    private Vector position;
    private Constraints constraints;
    private Constraints support;
    private Set<ConcentratedForce> concentratedForces;
    private Set<Moment> moments;

    private Joint(Builder builder) {
        id = builder.id;
        position = builder.position;
        constraints = builder.constraints;
        support = builder.support;
        concentratedForces = builder.concentratedForces;
        moments = builder.moments;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public Set<ConcentratedForce> getConcentratedForces() {
        return concentratedForces;
    }

    @Override
    public Set<Moment> getMoments() {
        return moments;
    }

    @Override
    public String toString() {
        return "Joint{" + "id='" + id + '\'' + ", position=" + position + ", constraints=" + constraints + ", support=" + support + ", " +
                "concentratedForces=" + concentratedForces + ", concentratedMoments=" + moments + '}';
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, position, constraints, support, concentratedForces, moments);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final Joint other = (Joint) obj;
        return Objects.equals(this.id, other.id) && Objects.equals(this.position, other.position) && Objects.equals(this.constraints,
                other.constraints) && Objects.equals(this.support, other.support) && Objects.equals(this.concentratedForces,
                other.concentratedForces) && Objects.equals(this.moments, other.moments);
    }

    public Vector getPosition() {
        return position;
    }

    public Constraints getConstraints() {
        return constraints;
    }

    public Constraints getSupport() {
        return support;
    }

    public static class Builder {

        private String id;
        private Vector position;
        private Constraints constraints;
        private Constraints support;
        private Set<ConcentratedForce> concentratedForces = new HashSet<>();
        private Set<Moment> moments = new HashSet<>();

        /**
         * Creates a new {@link Joint.Builder} instance with the given joint ID, position, and constraints.
         * @param id ID of the joint
         * @param position position of the joint
         * @param constraints constraints the joint applies on connected beams
         */
        public Builder(String id, Vector position, Constraints constraints) {
            this.id = requireNonNull(id, "ID must not be null.");
            this.position = requireNonNull(position, "Position must not be null.");
            this.constraints = requireNonNull(constraints, "Constraints must not be null.");
        }

        /**
         * Defines constraints on the joint.
         * @param support constraints on the joint
         * @return the builder instance
         */
        public Builder withSupport(Constraints support) {
            this.support = requireNonNull(support, "Support must not be null.");
            return this;
        }

        /**
         * Places a concentrated force on the joint.
         * @param concentratedForce a concentrated force
         * @return the builder instance
         */
        public Builder addConcentratedForce(ConcentratedForce concentratedForce) {
            requireNonNull(concentratedForce, "Concentrated force must not be null.");
            this.concentratedForces.add(concentratedForce);
            return this;
        }

        /**
         * Places a moment on the joint.
         * @param moment a moment
         * @return the builder instance
         */
        public Builder addMoment(Moment moment) {
            requireNonNull(moment, "Moment must not be null.");
            this.moments.add(moment);
            return this;
        }

        /**
         * Builds a {@link Joint} instance.
         * @return a {@link Joint} instance
         */
        public Joint build() {
            return new Joint(this);
        }

    }

}
