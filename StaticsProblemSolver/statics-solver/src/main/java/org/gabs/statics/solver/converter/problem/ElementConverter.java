package org.gabs.statics.solver.converter.problem;

import org.gabs.statics.model.problem.Element;
import org.gabs.statics.model.problem.Joint;
import org.gabs.statics.solver.converter.Converter;

/**
 * A {@link Converter} implementation that converts {@code StaticsApi} {@link Element}s to internal {@link org.gabs.statics.solver.model.Element Element} representation.
 */
class ElementConverter implements Converter<Element, org.gabs.statics.solver.model.Element> {

    @Override
    public org.gabs.statics.solver.model.Element convert(Element source) {
        return new org.gabs.statics.solver.model.Element(source.getId(), source instanceof Joint);
    }

}
