package org.gabs.statics.solver.resultbuilder.postprocessor;

import org.gabs.statics.model.common.Vector;
import org.gabs.statics.model.common.ConcentratedForce;
import org.gabs.statics.model.common.Moment;
import org.gabs.statics.model.result.Reaction;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class RoundingStrategyImplTest {

    private static final Vector POINT_OF_APPLICATION = new Vector(1, 2, 3);
    private String ELEMENT = "element";

    private RoundingStrategyImpl strategy;
    private Reaction result;

    @Test
    public void testRoundWithConcentratedForceAndPrecisionToRoundDown() {
        givenRoundingStrategyImpl();
        whenCalledWithConcentratedForceAndPrecisionToRoundDown();
        thenResultShouldHaveUnchangedSource();
        thenResultShouldHaveUnchangedPointOfApplication();
        thenResultShouldHaveForceProjectionsRoundedDown();
    }

    @Test
    public void testRoundWithConcentratedForceAndPrecisionToRoundUp() {
        givenRoundingStrategyImpl();
        whenCalledWithConcentratedForceAndPrecisionToRoundUp();
        thenResultShouldHaveUnchangedSource();
        thenResultShouldHaveUnchangedPointOfApplication();
        thenResultShouldHaveForceProjectionsRoundedUp();
    }

    @Test
    public void testRoundWithConcentratedForceAndPrecisionToRoundDownToEven() {
        givenRoundingStrategyImpl();
        whenCalledWithConcentratedForceAndPrecisionToRoundDownToEven();
        thenResultShouldHaveUnchangedSource();
        thenResultShouldHaveUnchangedPointOfApplication();
        thenResultShouldHaveForceProjectionsRoundedDownToEven();
    }

    @Test
    public void testRoundWithConcentratedForceAndPrecisionToRoundUpToEven() {
        givenRoundingStrategyImpl();
        whenCalledWithConcentratedForceAndPrecisionToRoundUpToEven();
        thenResultShouldHaveUnchangedSource();
        thenResultShouldHaveUnchangedPointOfApplication();
        thenResultShouldHaveForceProjectionsRoundedUpToEven();
    }

    @Test
    public void testRoundWithConcentratedMomentAndPrecisionToRoundDown() {
        givenRoundingStrategyImpl();
        whenCalledWithConcentratedMomentAndPrecisionToRoundDown();
        thenResultShouldHaveUnchangedSource();
        thenResultShouldHaveMomentProjectionsRoundedDown();
    }

    @Test
    public void testRoundWithConcentratedMomentAndPrecisionToRoundUp() {
        givenRoundingStrategyImpl();
        whenCalledWithConcentratedMomentAndPrecisionToRoundUp();
        thenResultShouldHaveUnchangedSource();
        thenResultShouldHaveMomentProjectionsRoundedUp();
    }

    @Test
    public void testRoundWithConcentratedMomentAndPrecisionToRoundDownToEven() {
        givenRoundingStrategyImpl();
        whenCalledWithConcentratedMomentAndPrecisionToRoundDownToEven();
        thenResultShouldHaveUnchangedSource();
        thenResultShouldHaveMomentProjectionsRoundedDownToEven();
    }

    @Test
    public void testRoundWithConcentratedMomentAndPrecisionToRoundUpToEven() {
        givenRoundingStrategyImpl();
        whenCalledWithConcentratedMomentAndPrecisionToRoundUpToEven();
        thenResultShouldHaveUnchangedSource();
        thenResultShouldHaveMomentProjectionsRoundedUpToEven();
    }

    private void givenRoundingStrategyImpl() {
        strategy = new RoundingStrategyImpl();
    }

    private void whenCalledWithConcentratedForceAndPrecisionToRoundDown() {
        Reaction reaction = new Reaction(ELEMENT, new ConcentratedForce(POINT_OF_APPLICATION, new Vector(0.12, 0.12, 0.12)), null);
        result = strategy.round(reaction, 1);
    }

    private void thenResultShouldHaveUnchangedSource() {
        assertThat(result.getSource(), equalTo(ELEMENT));
    }

    private void thenResultShouldHaveUnchangedPointOfApplication() {
        assertThat(result.getConcentratedForce().getPointOfApplication(), equalTo(POINT_OF_APPLICATION));
    }

    private void thenResultShouldHaveForceProjectionsRoundedDown() {
        ConcentratedForce concentratedForce = result.getConcentratedForce();
        assertThat(concentratedForce.getVector().getX(), equalTo(0.1));
        assertThat(concentratedForce.getVector().getY(), equalTo(0.1));
        assertThat(concentratedForce.getVector().getZ(), equalTo(0.1));
    }

    private void whenCalledWithConcentratedForceAndPrecisionToRoundUp() {
        Reaction reaction = new Reaction(ELEMENT, new ConcentratedForce(POINT_OF_APPLICATION, new Vector(0.18, 0.18, 0.18)), null);
        result = strategy.round(reaction, 1);
    }

    private void thenResultShouldHaveForceProjectionsRoundedUp() {
        ConcentratedForce concentratedForce = result.getConcentratedForce();
        assertThat(concentratedForce.getVector().getX(), equalTo(0.2));
        assertThat(concentratedForce.getVector().getY(), equalTo(0.2));
        assertThat(concentratedForce.getVector().getZ(), equalTo(0.2));
    }

    private void whenCalledWithConcentratedForceAndPrecisionToRoundDownToEven() {
        Reaction reaction = new Reaction(ELEMENT, new ConcentratedForce(POINT_OF_APPLICATION, new Vector(0.25, 0.25, 0.25)), null);
        result = strategy.round(reaction, 1);
    }

    private void thenResultShouldHaveForceProjectionsRoundedDownToEven() {
        ConcentratedForce concentratedForce = result.getConcentratedForce();
        assertThat(concentratedForce.getVector().getX(), equalTo(0.2));
        assertThat(concentratedForce.getVector().getY(), equalTo(0.2));
        assertThat(concentratedForce.getVector().getZ(), equalTo(0.2));
    }

    private void whenCalledWithConcentratedForceAndPrecisionToRoundUpToEven() {
        Reaction reaction = new Reaction(ELEMENT, new ConcentratedForce(POINT_OF_APPLICATION, new Vector(0.351, 0.351, 0.351)), null);
        result = strategy.round(reaction, 1);
    }

    private void thenResultShouldHaveForceProjectionsRoundedUpToEven() {
        ConcentratedForce concentratedForce = result.getConcentratedForce();
        assertThat(concentratedForce.getVector().getX(), equalTo(0.4));
        assertThat(concentratedForce.getVector().getY(), equalTo(0.4));
        assertThat(concentratedForce.getVector().getZ(), equalTo(0.4));
    }

    private void whenCalledWithConcentratedMomentAndPrecisionToRoundDown() {
        Reaction reaction = new Reaction(ELEMENT, null, new Moment(new Vector(0.12, 0.12, 0.12)));
        result = strategy.round(reaction, 1);
    }

    private void thenResultShouldHaveMomentProjectionsRoundedDown() {
        Moment moment = result.getMoment();
        assertThat(moment.getVector().getX(), equalTo(0.1));
        assertThat(moment.getVector().getY(), equalTo(0.1));
        assertThat(moment.getVector().getZ(), equalTo(0.1));
    }

    private void whenCalledWithConcentratedMomentAndPrecisionToRoundUp() {
        Reaction reaction = new Reaction(ELEMENT, null, new Moment(new Vector(0.18, 0.18, 0.18)));
        result = strategy.round(reaction, 1);
    }

    private void thenResultShouldHaveMomentProjectionsRoundedUp() {
        Moment concentratedForce = result.getMoment();
        assertThat(concentratedForce.getVector().getX(), equalTo(0.2));
        assertThat(concentratedForce.getVector().getY(), equalTo(0.2));
        assertThat(concentratedForce.getVector().getZ(), equalTo(0.2));
    }

    private void whenCalledWithConcentratedMomentAndPrecisionToRoundDownToEven() {
        Reaction reaction = new Reaction(ELEMENT, null, new Moment(new Vector(0.25, 0.25, 0.25)));
        result = strategy.round(reaction, 1);
    }

    private void thenResultShouldHaveMomentProjectionsRoundedDownToEven() {
        Moment moment = result.getMoment();
        assertThat(moment.getVector().getX(), equalTo(0.2));
        assertThat(moment.getVector().getY(), equalTo(0.2));
        assertThat(moment.getVector().getZ(), equalTo(0.2));
    }

    private void whenCalledWithConcentratedMomentAndPrecisionToRoundUpToEven() {
        Reaction reaction = new Reaction(ELEMENT, null, new Moment(new Vector(0.351, 0.351, 0.351)));
        result = strategy.round(reaction, 1);
    }

    private void thenResultShouldHaveMomentProjectionsRoundedUpToEven() {
        Moment moment = result.getMoment();
        assertThat(moment.getVector().getX(), equalTo(0.4));
        assertThat(moment.getVector().getY(), equalTo(0.4));
        assertThat(moment.getVector().getZ(), equalTo(0.4));
    }

}
