package org.gabs.statics.solver.matrix.builder.processor;

import org.gabs.statics.solver.model.Structure;
import org.gabs.statics.solver.matrix.builder.processor.constraint.ConstraintProcessor;
import org.gabs.statics.solver.matrix.builder.processor.load.LoadProcessor;
import org.gabs.statics.solver.model.Constraint;
import org.gabs.statics.solver.model.Dof;
import org.gabs.statics.solver.model.Effect;
import org.gabs.statics.solver.model.Element;
import org.gabs.statics.solver.registry.ConstraintRegistry;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumMap;
import java.util.Map;

/**
 * Implementation of the {@link ElementProcessor} interface that uses {@link ConstraintProcessor}s and {@link LoadProcessor}s to compose the equilibrium equations of a structural
 * element.
 */
class ElementProcessorImpl implements ElementProcessor {

    private ConstraintProcessor constraintProcessor;
    private LoadProcessor loadProcessor;

    @Override
    public Collection<double[]> process(Element element, Structure structure, Collection<Effect> load, ConstraintRegistry constraintRegistry) {
        Map<Dof, double[]> coefficients = createCoefficients(constraintRegistry);
        processConstraints(element, structure, constraintRegistry, coefficients);
        processLoad(load, coefficients, element.isJoint());
        return getFilteredCoefficients(coefficients);
    }

    private Map<Dof, double[]> createCoefficients(ConstraintRegistry constraintRegistry) {
        int numberOfCoefficientsWithConstant = constraintRegistry.size() + 1;
        Map<Dof, double[]> result = new EnumMap<>(Dof.class);
        result.put(Dof.X, new double[numberOfCoefficientsWithConstant]);
        result.put(Dof.Y, new double[numberOfCoefficientsWithConstant]);
        result.put(Dof.ZZ, new double[numberOfCoefficientsWithConstant]);
        return result;
    }

    private void processConstraints(Element element, Structure structure, ConstraintRegistry constraintRegistry, Map<Dof, double[]> coefficients) {
        for (Collection<Constraint> constraints : structure.getConstraintsOf(element)) {
            for (Constraint constraint : constraints) {
                constraintProcessor.process(constraint, constraintRegistry.getIndex(constraint), element.isJoint(), coefficients);
            }
        }
    }

    private void processLoad(Collection<Effect> load, Map<Dof, double[]> coefficients, boolean joint) {
        if (load != null) {
            for (Effect effect : load) {
                loadProcessor.process(effect, coefficients, joint);
            }
        }
    }

    private Collection<double[]> getFilteredCoefficients(Map<Dof, double[]> coefficientMap) {
        Collection<double[]> result = new ArrayList<>();
        for (double[] coefficients : coefficientMap.values()) {
            if (hasNonZeroValue(coefficients)) {
                result.add(coefficients);
            }
        }
        return result;
    }

    private boolean hasNonZeroValue(double[] array) {
        boolean result = false;
        for (double element : array) {
            if (element != 0) {
                result = true;
                break;
            }
        }
        return result;
    }

    @Inject
    public void setConstraintProcessor(ConstraintProcessor constraintProcessor) {
        this.constraintProcessor = constraintProcessor;
    }

    @Inject
    public void setLoadProcessor(LoadProcessor loadProcessor) {
        this.loadProcessor = loadProcessor;
    }

}
