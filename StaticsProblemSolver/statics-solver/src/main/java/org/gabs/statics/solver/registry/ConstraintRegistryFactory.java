package org.gabs.statics.solver.registry;

import org.gabs.statics.solver.model.Structure;

/**
 * Interface for building constraint registries.
 */
public interface ConstraintRegistryFactory {

    /**
     * Returns a constraint registry that has all the constraints of the given structure registered.
     * @param structure a structure
     * @return a constraint registry
     */
    ConstraintRegistry createFrom(Structure structure);

}
