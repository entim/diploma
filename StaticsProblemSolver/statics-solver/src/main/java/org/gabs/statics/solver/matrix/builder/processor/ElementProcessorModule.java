package org.gabs.statics.solver.matrix.builder.processor;

import com.google.inject.AbstractModule;

public class ElementProcessorModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(ElementProcessor.class).to(ElementProcessorImpl.class);
    }

}
