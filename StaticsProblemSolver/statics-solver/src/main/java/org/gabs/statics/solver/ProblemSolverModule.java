package org.gabs.statics.solver;

import com.google.inject.AbstractModule;

public class ProblemSolverModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(ProblemSolver.class).to(ProblemSolverImpl.class);
    }

}
